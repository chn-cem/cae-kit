#ifndef FAR_FIELD_SET_DIALOG_H
#define FAR_FIELD_SET_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_FarsDialog.h"
#include "DataBase.h"

class FarsDialog : public QDialog
{
	Q_OBJECT

public:
	
	FarsDialog(QWidget *parent = Q_NULLPTR);
	~FarsDialog() {}

	Ui::FarsDialog ui;

	void fn_AddFarsSet(std::vector<PreFarsSet> &vFar, QTreeWidgetItem *pRoot);
	void fn_EditFarSet(std::vector<PreFarsSet> &vFar, QTreeWidgetItem *pNode, int &iFar);

private:
	std::vector<PreFarsSet> *m_vFar{ nullptr };
	QTreeWidgetItem *m_pFarNode{ nullptr };
	int m_iCur{ -1 };

	void fn_SetFarValue(PreFarsSet &far);
	bool fn_IsNameExist(const std::string &sName, int iSkip);

public slots:
	void fn_QuickSetXOZ();
	void fn_QuickSetYOZ();
	void fn_QuickSetXOY();
	void fn_QuickSetSph();
	void fn_AddFarClose();
	void fn_AddFarApply();
	void fn_EditFarClose();
};

#endif