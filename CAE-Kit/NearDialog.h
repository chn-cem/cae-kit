#ifndef NEAR_FIELD_SET_DIALOG_H
#define NEAR_FIELD_SET_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_NearDialog.h"
#include "DataBase.h"

class NearDialog : public QDialog
{
	Q_OBJECT

public:

	NearDialog(QWidget *parent = Q_NULLPTR);
	~NearDialog() {}

	Ui::NearDialog ui;

	void fn_AddNearSet(std::vector<PreNearSet> &vNear, QTreeWidgetItem *pRoot);
	void fn_EditNearSet(std::vector<PreNearSet> &vNear, QTreeWidgetItem *pNode, int &iNear);

private:
	std::vector<PreNearSet> *m_vNear{ nullptr };
	QTreeWidgetItem *m_pNearNode{ nullptr };
	int m_iCur{ -1 };

	bool fn_IsNameExist(const std::string& sName, int iSkip);
	void fn_SetNearValue(PreNearSet &near);
	bool fn_LoadNearMesh(PreNearSet &near);

private slots:
	void fn_CheckRotation();
	void fn_AddNearClose();
	void fn_AddNearApply();
	void fn_EditNearClose();

	void fn_UpdateNumX(QString arg);
	void fn_UpdateNumY(QString arg);
	void fn_UpdateNumZ(QString arg);
	void fn_GetMshFile();
};

#endif