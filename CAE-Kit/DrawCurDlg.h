#ifndef DRAW_CURRENT_DIALOG_H
#define DRAW_CURRENT_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_DrawNfcDlg.h"
#include "./ThirdParty/QMultiComboBox.h"
#include "PostData.h"

class DrawCurDlg : public QDialog
{
	Q_OBJECT

public:
	DrawCurDlg(QWidget *parent = Q_NULLPTR);
	~DrawCurDlg();

	Ui::DrawNfcDlg ui;

	void fn_ShowDialog(PostData &post, CurSet &near);

private:
	QMultiComboBox *nearIndex{ nullptr };
	PostData *m_post{ nullptr };
	CurSet *m_pCurSet{ nullptr };


public slots:
	void fn_GetCurPara();
	void fn_SelectEorH();
	void fn_ChangePart();
};

#endif