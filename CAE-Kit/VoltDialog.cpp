
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>
#include <map>
#include "VoltDialog.h"

VoltDialog::VoltDialog(QWidget *parent)
{
	ui.setupUi(this);

	connect(ui.volt, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(fn_ShowRightMenu(const QPoint&)));
	connect(ui.actionAddVolt, SIGNAL(triggered()), this, SLOT(fn_AddVoltage()));
	connect(ui.actionDelVolt, SIGNAL(triggered()), this, SLOT(fn_DelVoltage()));
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_SaveVoltage()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

VoltDialog::~VoltDialog()
{
	ui.volt->clear();
}

void VoltDialog::fn_ShowVoltage(DataBase &data)
{
	QTableWidgetItem* item0, * item1;
	int n, m;

	m_pData = &data;

	ui.volt->setRowCount(data.fn_GetSourceNum());
	ui.volt->setColumnCount(2 * data.m_iNumVolt);

	QStringList list;
	for (n = 0; n < data.m_iNumVolt; ++n) {
		list.push_back("( Mag");
		list.push_back("Phase )");
	    for (m = 0; m < ui.volt->rowCount(); ++m) {
			item0 = new QTableWidgetItem;
			item1 = new QTableWidgetItem;
			item0->setText(QString::number(1.0));
			item1->setText(QString::number(0.0));
			ui.volt->setItem(m, 2 * n, item0);
			ui.volt->setItem(m, 2 * n + 1, item1);
		}
	}
	ui.volt->setHorizontalHeaderLabels(list);

	list.clear();
	m = 0;
	for (auto &src : data.m_vWave) {
		list.push_back(QString::fromStdString(src.sName));
		for (n = 0; n < src.vMag.size(); ++n) {
			ui.volt->item(m, 2 * n)->setText(QString::number(src.vMag[n]));
			ui.volt->item(m, 2 * n+1)->setText(QString::number(src.vPha[n]));
		}
		++m;
	}
	for (auto &src : data.m_vWGap) {
		list.push_back(QString::fromStdString(src.sName));
		for (n = 0; n < src.vMag.size(); ++n) {
			ui.volt->item(m, 2 * n)->setText(QString::number(src.vMag[n]));
			ui.volt->item(m, 2 * n + 1)->setText(QString::number(src.vPha[n]));
		}
		++m;
	}
	for (auto &src : data.m_vLump) {
		list.push_back(QString::fromStdString(src.sName));
		for (n = 0; n < src.vMag.size(); ++n) {
			ui.volt->item(m, 2 * n)->setText(QString::number(src.vMag[n]));
			ui.volt->item(m, 2 * n + 1)->setText(QString::number(src.vPha[n]));
		}
		++m;
	}
	for (auto &src : data.m_vPort) {
		list.push_back(QString::fromStdString(src.sName));
		for (n = 0; n < src.vMag.size(); ++n) {
			ui.volt->item(m, 2 * n)->setText(QString::number(src.vMag[n]));
			ui.volt->item(m, 2 * n + 1)->setText(QString::number(src.vPha[n]));
		}
		++m;
	}
	for (auto &src : data.m_vEqui) {
		list.push_back(QString::fromStdString(src.sName));
		for (n = 0; n < src.vMag.size(); ++n) {
			ui.volt->item(m, 2 * n)->setText(QString::number(src.vMag[n]));
			ui.volt->item(m, 2 * n + 1)->setText(QString::number(src.vPha[n]));
		}
		++m;
	}
	ui.volt->setVerticalHeaderLabels(list);
	this->setModal(true);
	this->show();
}

// deal with delete key press
void VoltDialog::keyPressEvent(QKeyEvent *p)
{
	if (p->key() == Qt::Key_Delete) {
		// delete element in table
		fn_DelVoltage();
	}
}

void VoltDialog::fn_ShowRightMenu(const QPoint &pos)
{
	auto popMenu = new QMenu(this);

	popMenu->addAction(ui.actionAddVolt);
	popMenu->addAction(ui.actionDelVolt);
	popMenu->exec(QCursor::pos());
}

void VoltDialog::fn_AddVoltage()
{
	QTableWidgetItem *item0, *item1;

	if (ui.volt->rowCount() <= 0) {
		// there is no source
		return;
	}
	// insert group
	int n = ui.volt->columnCount();
	ui.volt->setColumnCount(n + 2);
	auto col = new QTableWidgetItem("( Mag");
	ui.volt->setHorizontalHeaderItem(n, col);
	col = new QTableWidgetItem("Phase )");
	ui.volt->setHorizontalHeaderItem(n + 1, col);

	int m = ui.volt->rowCount();
	for (int i = 0; i < m; ++i) {
		item0 = new QTableWidgetItem;
		item1 = new QTableWidgetItem;
		item0->setText(QString::number(1.0));
		item1->setText(QString::number(0.0));
		ui.volt->setItem(i, n, item0);
		ui.volt->setItem(i, n + 1, item1);
	}
}

void VoltDialog::fn_DelVoltage()
{
	if (ui.volt->columnCount() > 2) {
		// delete element in table
		int n = ui.volt->currentColumn();
		if ("( Mag" == ui.volt->horizontalHeaderItem(n)->text()) {
			ui.volt->removeColumn(n + 1);
			ui.volt->removeColumn(n);
		} else {
			ui.volt->removeColumn(n);
			ui.volt->removeColumn(n - 1);
		}
	} else {
		QMessageBox::information(this, "Warn", "There must be at least one group voltage.");
	}
}

void VoltDialog::fn_SaveVoltage()
{
	int m, n, i, j;

	m_pData->m_iNumVolt = ui.volt->columnCount() / 2;

	m = 0;
	for (auto &src : m_pData->m_vWave) {
		src.vMag.resize(m_pData->m_iNumVolt, 1.0);
		src.vPha.resize(m_pData->m_iNumVolt, 0.0);

		for(n = 0; n < m_pData->m_iNumVolt; ++n){
			i = 2 * n;
			j = i + 1;
			src.vMag[n] = ui.volt->item(m, i)->text().toDouble();
			src.vPha[n] = ui.volt->item(m, j)->text().toDouble();
		}
		++m;
	}
	for (auto &src : m_pData->m_vWGap) {
		src.vMag.resize(m_pData->m_iNumVolt, 1.0);
		src.vPha.resize(m_pData->m_iNumVolt, 0.0);

		for (n = 0; n < m_pData->m_iNumVolt; ++n) {
			i = 2 * n;
			j = i + 1;
			src.vMag[n] = ui.volt->item(m, i)->text().toDouble();
			src.vPha[n] = ui.volt->item(m, j)->text().toDouble();
		}
		++m;
	}
	for (auto &src : m_pData->m_vLump) {
		src.vMag.resize(m_pData->m_iNumVolt, 1.0);
		src.vPha.resize(m_pData->m_iNumVolt, 0.0);

		for (n = 0; n < m_pData->m_iNumVolt; ++n) {
			i = 2 * n;
			j = i + 1;
			src.vMag[n] = ui.volt->item(m, i)->text().toDouble();
			src.vPha[n] = ui.volt->item(m, j)->text().toDouble();
		}
		++m;
	}
	for (auto &src : m_pData->m_vPort) {
		src.vMag.resize(m_pData->m_iNumVolt, 1.0);
		src.vPha.resize(m_pData->m_iNumVolt, 0.0);

		for (n = 0; n < m_pData->m_iNumVolt; ++n) {
			i = 2 * n;
			j = i + 1;
			src.vMag[n] = ui.volt->item(m, i)->text().toDouble();
			src.vPha[n] = ui.volt->item(m, j)->text().toDouble();
		}
		++m;
	}
	for (auto &src : m_pData->m_vEqui) {
		src.vMag.resize(m_pData->m_iNumVolt, 1.0);
		src.vPha.resize(m_pData->m_iNumVolt, 0.0);

		for (n = 0; n < m_pData->m_iNumVolt; ++n) {
			i = 2 * n;
			j = i + 1;
			src.vMag[n] = ui.volt->item(m, i)->text().toDouble();
			src.vPha[n] = ui.volt->item(m, j)->text().toDouble();
		}
		++m;
	}
	this->reject();
}
