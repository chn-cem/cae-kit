#ifndef MPI_CONFIGUE_DIALOG_H
#define MPI_CONFIGUE_DIALOG_H

#include "ui_Mpi_Config.h"
#include "DataBase.h"

class Mpi_Config : public QDialog
{
	Q_OBJECT

public:
	Mpi_Config(QWidget* parent = Q_NULLPTR);
	~Mpi_Config() {}

	void fn_ShowMpiDialog();

private:
	Ui::Mpi_Config ui;

private slots:
	
};

#endif

