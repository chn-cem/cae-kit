#ifndef PLANEDIALOG_H
#define PLANEDIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_WaveDialog.h"
#include "DataBase.h"

class WaveDialog : public QDialog
{
	Q_OBJECT

public:
	
	WaveDialog(QWidget *parent = Q_NULLPTR);
	~WaveDialog() {}

	Ui::WaveDialog ui;

	void fn_AddPlaneWaves(std::vector<PlanWave> &vWave, QTreeWidgetItem *pRoot);
	void fn_EditPlaneWave(std::vector<PlanWave> &vWave, QTreeWidgetItem *pNode, int &iWave);

private:
	std::vector<PlanWave> *m_vWave{ nullptr };
	QTreeWidgetItem *m_pWaveNode{ nullptr };
	int m_iCurWave{ -1 };

	void fn_SetPlaneValue(PlanWave &wave);
	bool fn_IsNameExist(const std::string &sName, int iSkip);

private slots:
	void fn_CheckRotation();
	void fn_AddPlaneClose();
	void fn_AddPlaneApply();
	void fn_EditPlanClose();
};

#endif