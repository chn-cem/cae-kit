#ifndef VTK_IMAGE_3D_H
#define VTK_IMAGE_3D_H

#include <QtWidgets/QGridLayout>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>

#include "QVTKOpenGLNativeWidget.h"
#include "vtkSmartPointer.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"

#include <string>

class VtkImage3D
{
public:
	VtkImage3D(QString &sName);
	~VtkImage3D();

private:
	QString m_sName;
	QWidget *m_tab{ nullptr };
    QGridLayout *m_gridLy{ nullptr };
	QVTKOpenGLNativeWidget *m_qvtkWidget{ nullptr };

	vtkSmartPointer<vtkActor> m_actor{ nullptr };
	vtkSmartPointer<vtkRenderer> m_render{ nullptr };
	vtkSmartPointer<vtkPolyDataMapper> m_mapper{ nullptr };

public:
	void fn_FitView();
	void fn_SetName(QString &sName);
	void fn_SetMapper(vtkSmartPointer<vtkPolyDataMapper> map);
	void fn_AddGdmMsh(vtkSmartPointer<vtkPolyData> gdm);
	void fn_AddTetMsh(vtkSmartPointer<vtkUnstructuredGrid> tet);
	void fn_ToRender();
	void fn_SaveGraph(std::string fname);
	void fn_ShowHideMsh();
	void fn_ShowGridMsh();
	void fn_ShowNodeMsh();
	void fn_Clear();

	QString fn_GetName();
	QWidget *fn_GetTab();
	QVTKOpenGLNativeWidget* fn_GetVTKWidget();
};

#endif