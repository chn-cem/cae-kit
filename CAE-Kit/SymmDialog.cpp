#include "SymmDialog.h"

SymmDialog::SymmDialog(QWidget *parent)
{
    ui.setupUi(this);
    this->setFixedSize(this->width(), this->height());

	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_UpdateSymme()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

void SymmDialog::fn_SetSymmetry(int *pSymSet)
{
    m_pSymSet = pSymSet;

	for (int i = 0; i < 5; ++i) {
		if (pSymSet[0] == SYMM_TYPE[i]) {
			ui.yoz->setCurrentIndex(i);
		}
		if (pSymSet[1] == SYMM_TYPE[i]) {
			ui.xoz->setCurrentIndex(i);
		}
		if (pSymSet[2] == SYMM_TYPE[i]) {
			ui.xoy->setCurrentIndex(i);
		}
	}
	this->show();
}

void SymmDialog::fn_UpdateSymme()
{
	m_pSymSet[0] = SYMM_TYPE[ui.yoz->currentIndex()];
	m_pSymSet[1] = SYMM_TYPE[ui.xoz->currentIndex()];
	m_pSymSet[2] = SYMM_TYPE[ui.xoy->currentIndex()];
	this->reject();
}
