//=================================================================================//
// This software works as an Pre-process and post-process tool for CEM
// Author : Wang Yong
// Date   : 2020.10.28
//=================================================================================//
#include <QtWidgets/QApplication>
#include <QSurfaceFormat>

#include "vtkAutoInit.h"
#include "vtkOutputWindow.h"
#include "QVTKOpenGLNativeWidget.h"
// for VTK-7.0 and later
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);
VTK_MODULE_INIT(vtkRenderingContextOpenGL2)
//VTK_MODULE_INIT(vtkRenderingVolumeOpenGL2)

#include "MainWindow.h"

int main(int argc, char *argv[])
{
    // needed to ensure appropriate OpenGL context is created for VTK rendering.
    QSurfaceFormat::setDefaultFormat(QVTKOpenGLNativeWidget::defaultFormat());
    // close warning window
    vtkOutputWindow::SetGlobalWarningDisplay(0);

    QApplication app(argc, argv);
    //QApplication::setStyle("fusion");
    app.setQuitOnLastWindowClosed(true);

    MainWindow win;
    win.setAttribute(Qt::WA_DeleteOnClose, true);
    win.showMaximized();
    win.fn_ShowVtkWidget();

    return app.exec();
}
