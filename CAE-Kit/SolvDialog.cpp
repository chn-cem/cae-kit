#include "SolvDialog.h"

SolvDialog::SolvDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.femBox->setHidden(true);
	ui.mlfmaBox->setHidden(true);
	this->setFixedWidth(this->width());
	this->adjustSize();

	connect(ui.solverName, SIGNAL(currentIndexChanged(QString)), this, SLOT(fn_SelectCemAlgo(QString)));
	connect(ui.femSolver,  SIGNAL(currentIndexChanged(QString)), this, SLOT(fn_SelectFemSolv(QString)));
	connect(ui.drivenMode, SIGNAL(clicked()), this, SLOT(fn_CheckRotation()));
	connect(ui.eigenMode,  SIGNAL(clicked()), this, SLOT(fn_CheckRotation()));
	connect(ui.ok,     SIGNAL(clicked()), this, SLOT(fn_UpdateSolver()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

void SolvDialog::fn_SetSolver(SolvPara &solvSet)
{
	m_pSolvSet = &solvSet;

	if (solvSet.sAccuracy == "Fine") {
		ui.Gauss->setCurrentIndex(0);
	} else if (solvSet.sAccuracy == "Normal") {
		ui.Gauss->setCurrentIndex(1);
	} else {
		ui.Gauss->setCurrentIndex(2);
	}
	ui.solverName->setCurrentIndex(solvSet.iAlgType);
	
	if (solvSet.iAlgType == ALG_MLFMA) {
		ui.fmmIterStep->setText(QString::number(solvSet.iFmmIterNum));
		ui.fmmIterErr->setText(QString::number(solvSet.dFmmIterErr));
		ui.cubeSize->setText(QString::number(solvSet.dFmmBoxSize));
		ui.ratioIE->setText(QString::number(solvSet.dFmmIERatio));
		ui.fmmIterType->setCurrentText(QString::fromStdString(solvSet.sFmmItrType));
		ui.preCond->setCurrentText(QString::fromStdString(solvSet.sFmmPreCond));
	} else if (solvSet.iAlgType == ALG_FEM) {
		if (solvSet.bHobFEM) {
			ui.femHob->setChecked(true);
		} else {
			ui.femHob->setChecked(false);
		}
		if (solvSet.bMetalIn) {
			ui.metalInside->setChecked(true);
		} else {
			ui.metalInside->setChecked(false);
		}
		ui.unit->setCurrentText(QString::fromStdString(solvSet.sLengthUnit));
		ui.boundary->setCurrentText(QString::fromStdString(solvSet.sBoundary));
		ui.femSolver->setCurrentText(QString::fromStdString(solvSet.sFemSolver));
		ui.femIterStep->setText(QString::number(solvSet.iFemMaxIterNum));
		ui.femIterErr->setText(QString::number(solvSet.dFemIterErr));
		if (solvSet.bEigMode) {
			ui.eigenMode->setChecked(true);
			ui.eigModeNum->setText(QString::number(solvSet.iEigModeNum));
			ui.eigMinFreq->setText(QString::number(solvSet.dEigMinFreq));
			ui.eigModeNum->setEnabled(true);
			ui.eigMinFreq->setEnabled(true);
		} else {
			ui.drivenMode->setChecked(true);
		}
		if (solvSet.bAdaptMsh) {
			ui.femAdaptMsh->setChecked(true);
			ui.adaptRatio->setText(QString::number(solvSet.dAdaptRatio));
			ui.adaptMinStep->setText(QString::number(solvSet.iAdapMinNum));
			ui.adaptMinPass->setText(QString::number(solvSet.iAdaPassNum));
			ui.adaptMaxStep->setText(QString::number(solvSet.iAdapMaxNum));
			ui.adapDeltaErr->setText(QString::number(solvSet.dAdaptDelta));
		} else {
			ui.femAdaptMsh->setChecked(false);
		}
	} else {

	}
	this->show();
}

void SolvDialog::fn_SelectCemAlgo(QString sArg)
{
	if (sArg == "MoM") {
		ui.femBox->hide();
		ui.mlfmaBox->hide();
	} else if (sArg == "MLFMA") {
		ui.femBox->hide();
		ui.mlfmaBox->show();
	} else if (sArg == "FEM") {
		ui.femBox->show();
		ui.mlfmaBox->hide();
	} else {
		ui.femBox->hide();
		ui.mlfmaBox->hide();
	}
	this->adjustSize();
}

void SolvDialog::fn_SelectFemSolv(QString sArg)
{
	if (sArg == "Direct") {
		ui.femIterStep->setDisabled(true);
		ui.femIterErr->setDisabled(true);
	} else {
		ui.femIterStep->setEnabled(true);
		ui.femIterErr->setEnabled(true);
	}
}

void SolvDialog::fn_CheckRotation()
{
	if (ui.drivenMode->isChecked()) {
		ui.eigModeNum->setDisabled(true);
		ui.eigMinFreq->setDisabled(true);
	} else {
		ui.eigModeNum->setEnabled(true);
		ui.eigMinFreq->setEnabled(true);
	}
}

void SolvDialog::fn_UpdateSolver()
{
	m_pSolvSet->sAccuracy = ui.Gauss->currentText().toStdString();
	m_pSolvSet->iAlgType = ui.solverName->currentIndex();

	if (m_pSolvSet->iAlgType == ALG_MLFMA) {
		m_pSolvSet->iFmmIterNum = ui.fmmIterStep->text().toInt();
        m_pSolvSet->dFmmIterErr = ui.fmmIterErr->text().toDouble();
		m_pSolvSet->dFmmBoxSize = ui.cubeSize->text().toDouble();
		m_pSolvSet->dFmmIERatio = ui.ratioIE->text().toDouble();
		m_pSolvSet->sFmmItrType = ui.fmmIterType->currentText().toStdString();
		m_pSolvSet->sFmmPreCond = ui.preCond->currentText().toStdString();
	} else if (m_pSolvSet->iAlgType == ALG_FEM) {
		m_pSolvSet->bHobFEM = false;
		if (ui.femHob->isChecked()) {
			m_pSolvSet->bHobFEM = true;
		}
		m_pSolvSet->bMetalIn = false;
		if (ui.metalInside->isChecked()) {
			m_pSolvSet->bMetalIn = true;
		}
		m_pSolvSet->sLengthUnit = ui.unit->currentText().toStdString();
		m_pSolvSet->sBoundary = ui.boundary->currentText().toStdString();
		m_pSolvSet->sFemSolver = ui.femSolver->currentText().toStdString();
		m_pSolvSet->iFemMaxIterNum = ui.femIterStep->text().toInt();
		m_pSolvSet->dFemIterErr = ui.femIterErr->text().toDouble();

		if (ui.eigenMode->isChecked()) {
			m_pSolvSet->bEigMode = true;
			m_pSolvSet->iEigModeNum = ui.eigModeNum->text().toInt();
			m_pSolvSet->dEigMinFreq = ui.eigMinFreq->text().toDouble();
		} else {
			m_pSolvSet->bEigMode = false;
		}
		if (ui.femAdaptMsh->isChecked()) {
			m_pSolvSet->bAdaptMsh = true;
			m_pSolvSet->dAdaptRatio = ui.adaptRatio->text().toDouble();
			m_pSolvSet->iAdapMinNum = ui.adaptMinStep->text().toInt();
			m_pSolvSet->iAdaPassNum = ui.adaptMinPass->text().toInt();
			m_pSolvSet->iAdapMaxNum = ui.adaptMaxStep->text().toInt();
			m_pSolvSet->dAdaptDelta = ui.adapDeltaErr->text().toDouble();
		} else {
			m_pSolvSet->bAdaptMsh = false;
		}
	} else {

	}
	this->reject();
}
