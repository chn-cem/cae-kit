#include <fstream>
#include <algorithm>
#include "vtkPolyData.h"
#include "vtkPointData.h"
#include "vtkFloatArray.h"
#include "vtkLookupTable.h"
#include "PostData.h"
//#include <QtWidgets/QMessageBox>
using namespace std;

void PostData::fn_ClearData()
{
    m_iNumVolt = 0;
    m_bRCS = false;
    m_bNet = false;

    m_vFreq.clear();
    m_vFarPara.clear();
    m_vNearName.clear();
    m_vFar.clear();
    m_vNear.clear();
	m_vCurr.clear();
    m_vFar.shrink_to_fit();
    m_vNear.shrink_to_fit();
	m_vCurr.shrink_to_fit();

	m_Net.bHasLoad = false;
	m_Net.vS.clear();
	m_Net.vY.clear();
	m_Net.vZ.clear();

	m_cmsh.bHasLoad = false;
	m_cmsh.vR.clear();
	m_cmsh.vvWire.clear();
	m_cmsh.vvTria.clear();
	m_cmsh.vvQuad.clear();
}

void PostData::fn_SetPostData(DataBase &data)
{
    m_iNumVolt = data.m_iNumVolt;
    // get all frequency
    m_vFreq.clear();
    m_vFreq.push_back(data.m_freqSet.dFreq0);
    int iNum = (data.m_freqSet.iNumFreq > 1 ? data.m_freqSet.iNumFreq - 1 : 1);
    double freq;
    double delta = (data.m_freqSet.dMaxFreq - data.m_freqSet.dMinFreq) / iNum;
    for (int i = 0; i < data.m_freqSet.iNumFreq; ++i) {
        freq = data.m_freqSet.dMinFreq + i * delta;
        if (fabs(freq - data.m_freqSet.dFreq0) > 1.0e-8) {
            m_vFreq.push_back(freq);
        }
    }
    if (data.m_freqSet.sFreqUnit == "THz") {
        delta = 1000;
    } else if (data.m_freqSet.sFreqUnit == "MHz") {
        delta = 0.001;
    } else if (data.m_freqSet.sFreqUnit == "KHz") {
        delta = 1.0e-6;
    } else if (data.m_freqSet.sFreqUnit == "Hz") {
        delta = 1.0e-9;
    } else {
        delta = 1.0;
    }
    for (auto &f : m_vFreq) {
        f *= delta;
    }

    // set far field data array
    m_vFar.clear();
    m_vFarPara.clear();
    if (m_iNumVolt > 0 && !data.m_vFars.empty()) {
        // set far field angle
        m_vFarPara.resize(data.m_vFars.size());
        for (auto i = 0; i < data.m_vFars.size(); ++i) {
            m_vFarPara[i].sName = data.m_vFars[i].sName;
            m_vFarPara[i].vTheta.resize(data.m_vFars[i].iNumTh);
            iNum = (data.m_vFars[i].iNumTh > 1 ? data.m_vFars[i].iNumTh - 1 : 1);
            delta = (data.m_vFars[i].dMaxTh - data.m_vFars[i].dMinTh) / iNum;
            for (auto j = 0; j < data.m_vFars[i].iNumTh; ++j) {
                m_vFarPara[i].vTheta[j] = data.m_vFars[i].dMinTh + delta * j;
            }

            m_vFarPara[i].vPhi.resize(data.m_vFars[i].iNumPh);
            iNum = (data.m_vFars[i].iNumPh > 1 ? data.m_vFars[i].iNumPh - 1 : 1);
            delta = (data.m_vFars[i].dMaxPh - data.m_vFars[i].dMinPh) / iNum;
            for (auto j = 0; j < data.m_vFars[i].iNumPh; ++j) {
                m_vFarPara[i].vPhi[j] = data.m_vFars[i].dMinPh + delta * j;
            }
        }

        m_vFar.resize(m_iNumVolt);
        for (auto &far : m_vFar) {
            far.vSurfData.resize(data.m_vFars.size());
            for (auto &surf : far.vSurfData) {
                surf.vFreqFar.resize(m_vFreq.size());
            }
        }
    }
    // set near field data array
    m_vNear.clear();
	m_vNearName.clear();
    if (m_iNumVolt > 0 && !data.m_vNear.empty()) {
        m_vNearName.resize(data.m_vNear.size());
		for (auto i = 0; i < data.m_vNear.size(); ++i) {
			m_vNearName[i] = data.m_vNear[i].sName;
		}
        m_vNear.resize(m_iNumVolt);
        for (auto i = 0; i < m_iNumVolt; ++i) {
            m_vNear[i].vSurfData.resize(data.m_vNear.size());
            for (auto &surf : m_vNear[i].vSurfData) {
                surf.vFreqNear.resize(m_vFreq.size());
            }
        }
    }
	// set current data array
	m_vCurr.clear();
	if (m_iNumVolt > 0 && data.m_bCalcCurr) {
		m_vCurr.resize(m_iNumVolt);
		for (auto &cur : m_vCurr) {
			cur.vFreqCur.resize(m_vFreq.size());
		}
	}
    // check is there network parameter
    iNum = data.fn_GetPortNum();
    if (iNum > 0) {
        m_bNet = true;
    } else {
        m_bNet = false;
    }
    iNum += data.m_vEqui.size();
    // check is RCS or Gain
    if (iNum > 0) {
        m_bRCS = false;
    } else {
        m_bRCS = true;
    }
}

bool PostData::fn_LoadOneFar(std::string &sPostFile, int &iVolt)
{
    if (m_vFar[iVolt].bHasLoad) return true;

    std::string sName = sPostFile + "_" + to_string(iVolt + 1) + ".far";
    ifstream fp(sName);
    if (!fp.is_open()) return false;

    int iNumFreq, k, m, n;
    float freq, th, ph;
    DataFar *pFar;

	getline(fp, sName);
	int i = 0;
	while (!fp.eof()) {
		if (fp >> freq) {
			for (auto &surf : m_vFar[iVolt].vSurfData) {
				fp >> sName >> m >> n;
				k = m * n;
				pFar = &(surf.vFreqFar[i]);
				pFar->vEp.resize(k);
				pFar->vEt.resize(k);
				pFar->vAbsE.resize(k);
				pFar->vGain.resize(k);
				pFar->vDire.resize(k);
				for (auto j = 0; j < k; ++j) {
					fp >> ph >> th >> pFar->vEt[j].re >> pFar->vEt[j].im >> pFar->vEp[j].re >> pFar->vEp[j].im;
					fp >> pFar->vAbsE[j] >> pFar->vGain[j] >> pFar->vDire[j];
				}
			}
			++i;
		}
	}
    fp.close();
    m_vFar[iVolt].bHasLoad = true;
    return true;
}

bool PostData::fn_LoadOneNear(std::string &sPostFile, int &iVolt)
{
	if (m_vNear[iVolt].bHasLoad) return true;

	std::string sName = sPostFile + "_" + to_string(iVolt + 1) + ".nf";
	ifstream fp(sName);
	if (!fp.is_open()) return false;

	int iNumFreq, m;
	float freq;
	DataEH *pNear;

	fp >> iNumFreq;
	for (int i = 0; i < iNumFreq; ++i) {
		if (fp >> freq) {
			for (auto& surf : m_vNear[iVolt].vSurfData) {
				fp >> surf.sName >> m;
				pNear = &(surf.vFreqNear[i]);
				pNear->vEx.resize(m);
				pNear->vEy.resize(m);
				pNear->vEz.resize(m);
				pNear->vHx.resize(m);
				pNear->vHy.resize(m);
				pNear->vHz.resize(m);
				for (auto j = 0; j < m; ++j) {
					fp >> pNear->vEx[j].re >> pNear->vEx[j].im >> pNear->vEy[j].re >> pNear->vEy[j].im;
					fp >> pNear->vEz[j].re >> pNear->vEz[j].im >> pNear->vHx[j].re >> pNear->vHx[j].im;
					fp >> pNear->vHy[j].re >> pNear->vHy[j].im >> pNear->vHz[j].re >> pNear->vHz[j].im;
				}
			}
		} else {
			break;
		}
	}
	fp.close();
	m_vNear[iVolt].bHasLoad = true;
	return true;
}

bool PostData::fn_LoadOneCurr(std::string &sPostFile, int &iVolt)
{
	if (m_vCurr[iVolt].bHasLoad) return true;

	std::string sName = sPostFile + "_" + to_string(iVolt + 1) + ".cur";
	ifstream fp(sName);
	if (!fp.is_open()) return false;

	int iNumFreq, iNumPoint;
	float freq;
	DataEH *pCur;

	fp >> iNumFreq;
	for (int i = 0; i < iNumFreq; ++i) {
		pCur = &(m_vCurr[iVolt].vFreqCur[i]);
		if (fp >> freq >> iNumPoint) {
			pCur->vEx.resize(iNumPoint);
			pCur->vEy.resize(iNumPoint);
			pCur->vEz.resize(iNumPoint);
			pCur->vHx.resize(iNumPoint);
			pCur->vHy.resize(iNumPoint);
			pCur->vHz.resize(iNumPoint);
			for (auto j = 0; j < iNumPoint; ++j) {
				fp >> pCur->vEx[j].re >> pCur->vEx[j].im >> pCur->vEy[j].re >> pCur->vEy[j].im;
				fp >> pCur->vEz[j].re >> pCur->vEz[j].im >> pCur->vHx[j].re >> pCur->vHx[j].im;
				fp >> pCur->vHy[j].re >> pCur->vHy[j].im >> pCur->vHz[j].re >> pCur->vHz[j].im;
			}
		} else {
			break;
		}
	}
	fp.close();
	m_vCurr[iVolt].bHasLoad = true;
	return true;
}

bool PostData::fn_LoadNetPara(std::string &sPostFile)
{
	if (m_Net.bHasLoad) return true;

	std::string sName = sPostFile + ".net";
	ifstream fp(sName);
	if (!fp.is_open()) return false;

	int iNumFreq;
	fp >> sName >> iNumFreq >> m_Net.iNumPort;
	if (iNumFreq < 1 || m_Net.iNumPort < 1) return true;

	int m = m_Net.iNumPort * m_Net.iNumPort;
	m_Net.vFreq.resize(iNumFreq);
	m_Net.vS.resize(m);
	m_Net.vY.resize(m);
	m_Net.vZ.resize(m);
	for (auto i = 0; i < m; ++i) {
		m_Net.vS[i].resize(iNumFreq);
		m_Net.vY[i].resize(iNumFreq);
		m_Net.vZ[i].resize(iNumFreq);
	}
	// load S-Parameter
	for (auto i = 0; i < iNumFreq; ++i) {
		fp >> m_Net.vFreq[i];
		for (auto j = 0; j < m; ++j) {
			fp >> m_Net.vS[j][i].re >> m_Net.vS[j][i].im;
		}
	}
	// load Z-Parameter
	fp >> sName >> iNumFreq >> m_Net.iNumPort;
	for (auto i = 0; i < iNumFreq; ++i) {
		fp >> m_Net.vFreq[i];
		for (auto j = 0; j < m; ++j) {
			fp >> m_Net.vZ[j][i].re >> m_Net.vZ[j][i].im;
		}
	}
	// load Y-Parameter
	fp >> sName >> iNumFreq >> m_Net.iNumPort;
	for (auto i = 0; i < iNumFreq; ++i) {
		fp >> m_Net.vFreq[i];
		for (auto j = 0; j < m; ++j) {
			fp >> m_Net.vY[j][i].re >> m_Net.vY[j][i].im;
		}
	}
	fp.close();

	if (m_Net.vFreq.back() <= 1.0) {
		m_Net.sFreqUnit = "Frequency  (KHz)";
		for (auto i = 0; i < iNumFreq; ++i) m_Net.vFreq[i] *= 1000.0;
	} else if (m_Net.vFreq.back() <= 1000.0) {
		m_Net.sFreqUnit = "Frequency  (MHz)";
	} else {
		m_Net.sFreqUnit = "Frequency  (GHz)";
		for (auto i = 0; i < iNumFreq; ++i) m_Net.vFreq[i] /= 1000.0;
	}

	return true;
}

bool PostData::fn_LoadCurrMsh(std::string &sPostFile)
{
	if (m_cmsh.bHasLoad) return true;

	std::string sName = sPostFile + ".cmsh";
	ifstream fp(sName);
	if (!fp.is_open()) return false;

	int iNumNode, iNumWire, iNumTria, iNumQuad;
	fp >> iNumNode >> iNumWire >> iNumTria >> iNumQuad;
	if (iNumNode > 0) m_cmsh.vR.resize(iNumNode);
	if (iNumWire > 0) m_cmsh.vvWire.resize(iNumWire, vector<int>(2));
	if (iNumTria > 0) m_cmsh.vvTria.resize(iNumTria, vector<int>(3));
	if (iNumQuad > 0) m_cmsh.vvQuad.resize(iNumQuad, vector<int>(4));
	for (auto &r : m_cmsh.vR) {
		fp >> r.x >> r.y >> r.z;
	}
	for (auto &v : m_cmsh.vvWire) {
		fp >> v[0] >> v[1];
		--v[0];
		--v[1];
	}
	for (auto &v : m_cmsh.vvTria) {
		fp >> v[0] >> v[1] >> v[2];
		--v[0];
		--v[1];
		--v[2];
	}
	for (auto &v : m_cmsh.vvQuad) {
		fp >> v[0] >> v[1] >> v[2] >> v[3];
		--v[0];
		--v[1];
		--v[2];
		--v[3];
	}
	fp.close();

	m_cmsh.bHasLoad = true;
	return true;
}

vtkSmartPointer<vtkPolyDataMapper> PostData::fn_CreateMapper(FarSet &para, Node &Oc, float rad, float range)
{
    DataFar *far = &(m_vFar[para.iVoltIdx].vSurfData[para.iFarIndx].vFreqFar[para.iFrqIndx]);

	//创建对象
	auto object = vtkPolyData::New();
	auto points = vtkPoints::New();
	auto lines = vtkCellArray::New();
	auto polys = vtkCellArray::New();
	//存储标量值
	auto scalars = vtkFloatArray::New();

	vector<float> vValue;
	fn_GetFarValue(para, far, vValue);

	auto iNum = far->vGain.size();
	float dMaxVal = vValue[0];
	float dMinVal = vValue[0];
	for (auto i = 1; i < iNum; ++i) {
		if (vValue[i] < dMinVal) dMinVal = vValue[i];
		if (vValue[i] > dMaxVal) dMaxVal = vValue[i];
	}

	// 是否选择归一化
	if (para.bNormlize) {
		for (auto i = 0; i < iNum; ++i) vValue[i] /= dMaxVal;      // firstly, make all the data normalized
		dMinVal /= dMaxVal;
		dMaxVal = 1.0;
	}

	// 是否取dB
	float tmp;
	if (para.bSetdB) {
		float dBcoef;
		if ("Electric Field" == para.sQuantity || "Axial Ratio" == para.sQuantity) {
			dBcoef = 20.0;
		} else {
			dBcoef = 10.0;
		}
		dMinVal = dBcoef * log10(dMinVal);
		dMaxVal = dBcoef * log10(dMaxVal);
		if (dMaxVal - dMinVal > range) dMinVal = dMaxVal - range;

		for (auto i = 0; i < iNum; ++i) {
			tmp = dBcoef * log10(vValue[i]);
			if (tmp < dMinVal) {
				vValue[i] = dMinVal;
			} else {
				vValue[i] = tmp;
			}
		}
	}
	//存储顶点.  
	float rho, sinTh, sinPh, cosPh, r[3];
	auto iNumTh = m_vFarPara[para.iFarIndx].vTheta.size();
	auto iNumPh = m_vFarPara[para.iFarIndx].vPhi.size();
	vtkIdType k = 0;
	if (0 == para.iCoordinate) {           // sphere coordinate
		tmp = rad / (dMaxVal - dMinVal);
		for (auto ph : m_vFarPara[para.iFarIndx].vPhi) {
			ph *= (PI / 180.0);
			sinPh = sin(ph);
			cosPh = cos(ph);
			for (auto th : m_vFarPara[para.iFarIndx].vTheta) {
				th *= (PI / 180.0);
				sinTh = sin(th);
				rho = tmp * (vValue[k] - dMinVal);
				r[0] = rho * sinTh * cos(ph) + Oc.x;
				r[1] = rho * sinTh * sin(ph) + Oc.y;
				r[2] = rho * cos(th) + Oc.z;
				points->InsertPoint(k, r);
				++k;
			}
		}
	} else if (1 == para.iCoordinate) {    // 直角坐标系 coordinate
		if (abs(dMinVal) > abs(dMaxVal)) {
			tmp = abs(dMinVal);
		} else {
			tmp = abs(dMaxVal);
		}
		for (auto ph : m_vFarPara[para.iFarIndx].vPhi) {
			ph *= (PI / 180.0);
			for (auto th : m_vFarPara[para.iFarIndx].vTheta) {
				th *= (PI / 180.0);
				rho = vValue[k] * PI / tmp;
				r[0] = ph - PI + Oc.x;
				r[1] = th - PI / 2.0 + Oc.y;
				r[2] = rho;
				points->InsertPoint(k, r);
				++k;
			}
		}
	} else {                                  // couter
		for (auto ph : m_vFarPara[para.iFarIndx].vPhi) {
			ph *= (PI / 180.0);
			for (auto th : m_vFarPara[para.iFarIndx].vTheta) {
				th *= (PI / 180.0);
				r[0] = ph - PI + Oc.x;
				r[1] = th - PI / 2.0 + Oc.y;
				r[2] = 0.0;
				points->InsertPoint(k, r);
				++k;
			}
		}
	}
	// insert points
	object->SetPoints(points);

	//设定单元
	vtkIdType v[4];
	if (1 == iNumPh) {
		for (auto i = 1; i < iNumTh; ++i) {
			auto seg = vtkLine::New();
			seg->GetPointIds()->SetId(0, i - 1);
			seg->GetPointIds()->SetId(1, i);
			lines->InsertNextCell(seg);
		}
		// 设定单元类型为线段
		object->SetLines(lines);
	} else if (1 == iNumTh) {
		for (auto i = 1; i < iNumPh; ++i) {
			auto seg = vtkLine::New();
			seg->GetPointIds()->SetId(0, i - 1);
			seg->GetPointIds()->SetId(1, i);
			lines->InsertNextCell(seg);
		}
		// 设定单元类型为线段
		object->SetLines(lines);
	} else {
		vtkIdType iHead;
		for (auto i = 1; i < iNumPh; ++i) {
			iHead = i * iNumTh;
			for (auto j = 1; j < iNumTh; ++j) {
				v[0] = iHead - iNumTh + j - 1;
				v[1] = iHead - iNumTh + j;
				v[2] = iHead + j;
				v[3] = iHead + j - 1;
				polys->InsertNextCell(4, v);
			}
		}
		// 设定单元类型为多边形
		object->SetPolys(polys);
	}

	// 设置各点物理量数值
	for (auto i = 0; i < iNum; ++i) {
		scalars->InsertTuple1(i, vValue[i]);
	}
	vValue.clear();

	// set the scalar value of each node
	object->GetPointData()->SetScalars(scalars);
	auto map = vtkSmartPointer<vtkPolyDataMapper>::New();
	map->SetInputData(object);
	map->SetScalarRange(dMinVal, dMaxVal);

	//add colorbar
	auto pColorTable = vtkSmartPointer<vtkLookupTable>::New();
	pColorTable->SetHueRange(0.667, 0.0);
	map->SetLookupTable(pColorTable);
	pColorTable->Build();

	points->Delete();
	lines->Delete();
	polys->Delete();
	scalars->Delete();
	object->Delete();

	return map;
}

vtkSmartPointer<vtkPolyDataMapper> PostData::fn_CreateMapper(NearSet &para, DataBase &data, float range)
{
	//存储标量值
	auto scalars = vtkFloatArray::New();
	vector<float> vValue;
	for (auto &i : para.vNearIndx) {
		DataEH *pNear = &(m_vNear[para.iVoltIndx].vSurfData[i].vFreqNear[para.iFreqIndx]);
		fn_GetNfcValue(para.bIsE, para.sComponent, para.sPart, pNear, vValue);
	}
	float dMinVal = *min_element(vValue.begin(), vValue.end());
	float dMaxVal = *max_element(vValue.begin(), vValue.end());
	// 是否选择归一化
	if (para.bNormlize) {
		for (auto i = 0; i < vValue.size(); ++i) vValue[i] /= dMinVal;  // firstly, make all the data normalized   
		dMinVal = dMinVal / dMaxVal;
		dMaxVal = 1.0;
	}
	// 是否取dB
	if (para.bSetdB) {
		dMinVal = 20.0 * log10(dMinVal);
		dMaxVal = 20.0 * log10(dMaxVal);
		if (dMaxVal - dMinVal > range) dMinVal = dMaxVal - range;

		for (auto i = 0; i < vValue.size(); ++i) {
			vValue[i] = 20.0 * log10(vValue[i]);
			if (vValue[i] < dMinVal) {
				vValue[i] = dMinVal;
			}
		}
	}
	// 设置各点物理量数值
	for (auto i = 0; i < vValue.size(); ++i) {
		scalars->InsertTuple1(i, vValue[i]);
	}
	vValue.clear(); vValue.shrink_to_fit();

	//创建对象
	auto object = vtkPolyData::New();
	auto points = vtkPoints::New();
	auto lines = vtkCellArray::New();
	auto polys = vtkCellArray::New();

	// set the mesh of each near field
	vtkIdType n = 0, m;
	vtkIdType v[4];
	for (auto &i : para.vNearIndx) {
		m = n;
		if (data.m_vNear[i].bUserType) {
			fn_SetNearMesh(data.m_vNear[i], n, points, lines, polys);
		} else {
			for (auto &r : data.m_vNear[i].vR) {
				points->InsertPoint(n, r.x, r.y, r.z);
				++n;
			}
			for (auto &tria : data.m_vNear[i].vvTria) {
				v[0] = tria[0] + m - 1;
				v[1] = tria[1] + m - 1;
				v[2] = tria[2] + m - 1;
				polys->InsertNextCell(3, v);
			}
			for (auto &quad : data.m_vNear[i].vvQuad) {
				v[0] = quad[0] + m - 1;
				v[1] = quad[1] + m - 1;
				v[2] = quad[2] + m - 1;
				v[3] = quad[3] + m - 1;
				polys->InsertNextCell(4, v);
			}
		}
	}
	// insert points
	object->SetPoints(points);
	// insert lines
	object->SetLines(lines);
	// insert polygon
	object->SetPolys(polys);
	// set the scalar value of each node
	object->GetPointData()->SetScalars(scalars);
	auto map = vtkSmartPointer<vtkPolyDataMapper>::New();
	map->SetInputData(object);
	map->SetScalarRange(dMinVal, dMaxVal);

	//add colorbar
	auto pColorTable = vtkSmartPointer<vtkLookupTable>::New();
	pColorTable->SetHueRange(0.667, 0.0);
	map->SetLookupTable(pColorTable);
	pColorTable->Build();

	points->Delete();
	lines->Delete();
	polys->Delete();
	scalars->Delete();
	object->Delete();

	return map;
}

vtkSmartPointer<vtkPolyDataMapper> PostData::fn_CreateMapper(CurSet &para, DataBase &data, float range)
{
	//存储标量值
	auto scalars = vtkFloatArray::New();
	vector<float> vValue;
	DataEH *pCur = &(m_vCurr[para.iVoltIndx].vFreqCur[para.iFreqIndx]);
	fn_GetNfcValue(para.bIsJ, para.sComponent, para.sPart, pCur, vValue);
	float dMinVal = *min_element(vValue.begin(), vValue.end());
	float dMaxVal = *max_element(vValue.begin(), vValue.end());
	// 是否选择归一化
	if (para.bNormlize) {
		for (auto i = 0; i < vValue.size(); ++i) vValue[i] /= dMinVal;  // firstly, make all the data normalized   
		dMinVal = dMinVal / dMaxVal;
		dMaxVal = 1.0;
	}
	// 是否取dB
	if (para.bSetdB) {
		dMinVal = 20.0 * log10(dMinVal);
		dMaxVal = 20.0 * log10(dMaxVal);
		if (dMaxVal - dMinVal > range) dMinVal = dMaxVal - range;

		for (auto i = 0; i < vValue.size(); ++i) {
			vValue[i] = 20.0 * log10(vValue[i]);
			if (vValue[i] < dMinVal) {
				vValue[i] = dMinVal;
			}
		}
	}
	// 设置各点物理量数值
	for (auto i = 0; i < vValue.size(); ++i) {
		scalars->InsertTuple1(i, vValue[i]);
	}
	vValue.clear(); vValue.shrink_to_fit();

	//创建对象
	auto object = vtkPolyData::New();
	auto points = vtkPoints::New();
	auto lines = vtkCellArray::New();
	auto polys = vtkCellArray::New();

	// set the mesh
	vtkIdType n = 0;
	for (auto &r : m_cmsh.vR) {
		points->InsertPoint(n, r.x, r.y, r.z);
		++n;
	}
	for (auto &wire : m_cmsh.vvWire) {
		auto seg = vtkLine::New();
		seg->GetPointIds()->SetId(0, wire[0]);
		seg->GetPointIds()->SetId(1, wire[1]);
		lines->InsertNextCell(seg);
	}
	vtkIdType v[4];
	for (auto &tria : m_cmsh.vvTria) {
		v[0] = tria[0];
		v[1] = tria[1];
		v[2] = tria[2];
		polys->InsertNextCell(3, v);
	}
	for (auto &quad : m_cmsh.vvQuad) {
		v[0] = quad[0];
		v[1] = quad[1];
		v[2] = quad[2];
		v[3] = quad[3];
		polys->InsertNextCell(4, v);
	}
	
	
	// insert points
	object->SetPoints(points);
	// insert lines
	object->SetLines(lines);
	// insert polygon
	object->SetPolys(polys);
	// set the scalar value of each node
	object->GetPointData()->SetScalars(scalars);
	auto map = vtkSmartPointer<vtkPolyDataMapper>::New();
	map->SetInputData(object);
	map->SetScalarRange(dMinVal, dMaxVal);

	//add colorbar
	auto pColorTable = vtkSmartPointer<vtkLookupTable>::New();
	pColorTable->SetHueRange(0.667, 0.0);
	map->SetLookupTable(pColorTable);
	pColorTable->Build();

	points->Delete();
	lines->Delete();
	polys->Delete();
	scalars->Delete();
	object->Delete();

	return map;
}

vtkSmartPointer<vtkTable> PostData::fn_CreateTable(FarSet &para, string &xLabel, string &yLabel)
{
	DataFar *far = &(m_vFar[para.iVoltIdx].vSurfData[para.iFarIndx].vFreqFar[para.iFrqIndx]);

	// Create a table with some curves in it
	auto table = vtkSmartPointer<vtkTable>::New();
	// set the number of curves
	auto iNumCurve = para.vAngleIndx.size();
	// add X-axis
	auto arrX = vtkSmartPointer<vtkFloatArray>::New();
	int iNumPoint;
    float deg, dBcoef;
	if (para.bTheta) {
		xLabel = "Theta  (degree)";
		arrX->SetName("Theta");
		table->AddColumn(arrX);
		// set the value of each curve
		iNumPoint = m_vFarPara[para.iFarIndx].vTheta.size();;

		// add Y-axis
		for (auto &i : para.vAngleIndx) {
			auto arrY = vtkSmartPointer<vtkFloatArray>::New();
			deg = m_vFarPara[para.iFarIndx].vPhi[i];
			std::string name = "Phi = " + QString::number(deg).toStdString();
			arrY->SetName(name.c_str());
			table->AddColumn(arrY);
		}
		// set the point number of each curve
		table->SetNumberOfRows(iNumPoint);

		// add value of X-axis
		for (auto i = 0; i < iNumPoint; ++i) {
			table->SetValue(i, 0, m_vFarPara[para.iFarIndx].vTheta[i]);
		}
	} else {
		xLabel = "Phi  (degree)";
		arrX->SetName("Phi");
		table->AddColumn(arrX);
		// set the value of each curve
		iNumPoint = m_vFarPara[para.iFarIndx].vPhi.size();;

		// add Y-axis
		for (auto &i : para.vAngleIndx) {
			auto arrY = vtkSmartPointer<vtkFloatArray>::New();
			deg = m_vFarPara[para.iFarIndx].vTheta[i];
			std::string name = "Theta = " + QString::number(deg).toStdString();
			arrY->SetName(name.c_str());
			table->AddColumn(arrY);
		}
		// set the point number of each curve
		table->SetNumberOfRows(iNumPoint);

		// add value of X-axis
		for (auto i = 0; i < iNumPoint; ++i) {
			table->SetValue(i, 0, m_vFarPara[para.iFarIndx].vPhi[i]);
		}
	}

	// allocate memory
	auto pVal = new float*[iNumCurve];
	for (auto i = 0; i < iNumCurve; ++i) pVal[i] = new float[iNumPoint];

	// get the curve data
	fn_GetCurveFar(para, far, pVal);

	yLabel = para.sQuantity.toStdString();

	int i, j;
	float dMinVal = pVal[0][0];
	float dMaxVal = pVal[0][0];
	for (i = 0; i < iNumCurve; ++i) {
		for (j = 0; j < iNumPoint; ++j) {
			if (dMaxVal < pVal[i][j]) dMaxVal = pVal[i][j];
			if (dMinVal > pVal[i][j]) dMinVal = pVal[i][j];
		}
	}
	// 是否选择归一化
	if (para.bNormlize) {
		for (i = 0; i < iNumCurve; ++i) {
			for (j = 0; j < iNumPoint; ++j) {
				pVal[i][j] /= dMaxVal;      // firstly, make all the data normalized
			}
		}
		dMinVal /= dMaxVal;
		dMaxVal = 1.0;
	}

	// 是否取dB
	if (para.bSetdB) {
		if ("Electric Field" == para.sQuantity || "Axial Ratio" == para.sQuantity) {
			dBcoef = 20.0;
		} else {
			dBcoef = 10.0;
		}
		dMinVal = dBcoef * log10(dMinVal);
		dMaxVal = dBcoef * log10(dMaxVal);
		if (dMaxVal - dMinVal > 100.0) dMinVal = dMaxVal - 100.0;

		for (i = 0; i < iNumCurve; ++i) {
			for (j = 0; j < iNumPoint; ++j) {
				pVal[i][j] = dBcoef * log10(pVal[i][j]);
			}
		}
	}

	// add value of Y-axis
	for (i = 0; i < iNumCurve; ++i) {
		for (j = 0; j < iNumPoint; ++j) {
			table->SetValue(j, i + 1, pVal[i][j]);
		}
	}

	// release memory space
	for (i = 0; i < iNumCurve; i++) delete[] pVal[i];
	delete[] pVal;

	return table;
}

vtkSmartPointer<vtkTable> PostData::fn_CreateTable(NetSet &para, string &xLabel, string &yLabel)
{
	xLabel = m_Net.sFreqUnit;
	yLabel = para.sType + "-Parameter";
	// Create a table with some curves in it
	auto table = vtkSmartPointer<vtkTable>::New();

	int iNumCurve = para.vLabel.size();
	// add X-axis
	auto arrX = vtkSmartPointer<vtkFloatArray>::New();
	arrX->SetName("Frequency");
	table->AddColumn(arrX);
	// add Y-axis
	for (auto &name : para.vLabel) {
		auto arrY = vtkSmartPointer<vtkFloatArray>::New();
		arrY->SetName(name.c_str());
		table->AddColumn(arrY);
	}
	// set the point number of each curve
	table->SetNumberOfRows(m_Net.vFreq.size());
	// add value of X-axis
	for (auto i = 0; i < m_Net.vFreq.size(); ++i) {
		table->SetValue(i, 0, m_Net.vFreq[i]);
	}

	vector<vector<Complex>>* pData;
	if (para.sType == "S") {
		pData = &(m_Net.vS);
	} else if (para.sType == "Z") {
		pData = &(m_Net.vZ);
	} else if (para.sType == "Y") {
		pData = &(m_Net.vY);
	} else {
		pData = &(m_Net.vS);
	}
	
	// set the curve data
	float dMag, dPhase;
	for (auto i = 0; i < iNumCurve; ++i) {
		if (para.sType == "SWR") {
			int m = para.vPortI[i] * m_Net.iNumPort + para.vPortI[i];
			auto &vVal = (*pData)[m];
			for (auto j = 0; j < m_Net.vFreq.size(); ++j) {
				dMag = sqrt(vVal[j].re * vVal[j].re + vVal[j].im * vVal[j].im);
				dMag = (1.0 + dMag) / (1.0 - dMag);
				table->SetValue(j, i + 1, dMag);
			}
		} else {
			int m = para.vPortI[i] * m_Net.iNumPort + para.vPortJ[i];
			auto &vVal = (*pData)[m];
			if (0 == para.vPart[i]) {            // Real
				for (auto j = 0; j < m_Net.vFreq.size(); ++j) {
					table->SetValue(j, i + 1, vVal[j].re);
				}
			} else if (1 == para.vPart[i]) {     // Image
				for (auto j = 0; j < m_Net.vFreq.size(); ++j) {
					table->SetValue(j, i + 1, vVal[j].im);
				}
			} else if (2 == para.vPart[i]) {     // Mag
				for (auto j = 0; j < m_Net.vFreq.size(); ++j) {
					dMag = sqrt(vVal[j].re * vVal[j].re + vVal[j].im * vVal[j].im);
					table->SetValue(j, i + 1, dMag);
				}
			} else if (3 == para.vPart[i]) {     // dB
				for (auto j = 0; j < m_Net.vFreq.size(); ++j) {
					dMag = vVal[j].re * vVal[j].re + vVal[j].im * vVal[j].im;
					dMag = 10.0 * log10(dMag);
					table->SetValue(j, i + 1, dMag);
				}
			} else {                             // Phase
				for (auto j = 0; j < m_Net.vFreq.size(); ++j) {
					dPhase = fn_GetPhase(vVal[j].re, vVal[j].im);
					table->SetValue(j, i + 1, dPhase);
				}
			}
		}
	}

	return table;
}

void PostData::fn_GetFarValue(FarSet &para, DataFar *far, vector<float> &vValue)
{
	auto iNum = far->vGain.size();
	vValue.resize(iNum);
	float tmp, re, im;

	if ("Electric Field" == para.sQuantity) {
		if ("Abs" == para.sComponent) {               // 电场模值
			for (auto i = 0; i < iNum; ++i) {
				vValue[i] = far->vAbsE[i];
			}
		} else if ("Theta" == para.sComponent) {
			if ("Real" == para.sPart) {               // 电场theta分量实部
				for (auto i = 0; i < iNum; ++i) {
					vValue[i] = far->vEt[i].re;
				}
			} else if ("Image" == para.sPart) {       // 电场theta分量虚部
				for (auto i = 0; i < iNum; ++i) {
					vValue[i] = far->vEt[i].im;
				}
			} else if ("Mag" == para.sPart) {         // 电场theta分量模值
				for (auto i = 0; i < iNum; ++i) {
					tmp = far->vEt[i].re * far->vEt[i].re + far->vEt[i].im * far->vEt[i].im;
					vValue[i] = sqrt(tmp);
				}
			} else {
				for (auto i = 0; i < iNum; ++i) {   // 电场theta分量相位
					vValue[i] = fn_GetPhase(far->vEt[i].re, far->vEt[i].im);
				}
			}
		} else {
			if ("Real" == para.sPart) {
				for (auto i = 0; i < iNum; ++i) {   // 电场phi分量实部
					vValue[i] = far->vEp[i].re;
				}
			} else if ("Image" == para.sPart) {
				for (auto i = 0; i < iNum; ++i) {   // 电场phi分量虚部
					vValue[i] = far->vEp[i].im;
				}
			} else if ("Mag" == para.sPart) {
				for (auto i = 0; i < iNum; ++i) {   // 电场phi分量模值
					tmp = far->vEp[i].re * far->vEp[i].re + far->vEp[i].im * far->vEp[i].im;
					vValue[i] = sqrt(tmp);
				}
			} else {
				for (auto i = 0; i < iNum; ++i) {   // 电场phi分量相位
					vValue[i] = fn_GetPhase(far->vEp[i].re, far->vEp[i].im);
				}
			}
		}
	} else if ("RCS" == para.sQuantity || "Gain" == para.sQuantity) {
		if ("Abs" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // RCS或Gain模值
				vValue[i] = far->vGain[i];
			}
		} else if ("Theta" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // RCS或Gain的theta分量
				tmp = far->vEt[i].re * far->vEt[i].re + far->vEt[i].im * far->vEt[i].im;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vGain[i] * tmp;
			}
		} else if ("Phi" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // RCS或Gain的phi分量
				tmp = far->vEp[i].re * far->vEp[i].re + far->vEp[i].im * far->vEp[i].im;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vGain[i] * tmp;
			}
		} else if ("LHC" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // RCS或Gain的左旋分量
				re = far->vEt[i].re - far->vEp[i].im;
				im = far->vEt[i].im + far->vEp[i].re;
				tmp = (re * re + im * im) / 2.0;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vGain[i] * tmp;
			}
		} else {
			for (auto i = 0; i < iNum; ++i) {        // RCS或Gain的右旋分量
				re = far->vEt[i].re + far->vEp[i].im;
				im = far->vEt[i].im - far->vEp[i].re;
				tmp = (re * re + im * im) / 2.0;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vGain[i] * tmp;
			}
		}
	} else if ("Directivity" == para.sQuantity) {
		if ("Abs" == para.sComponent) {          // Directivity模值
			for (auto i = 0; i < iNum; ++i) {
				vValue[i] = far->vDire[i];
			}
		} else if ("Theta" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // Directivity的theta分量
				tmp = far->vEt[i].re * far->vEt[i].re + far->vEt[i].im * far->vEt[i].im;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vDire[i] * tmp;
			}
		} else if ("Phi" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // Directivity的phi分量
				tmp = far->vEp[i].re * far->vEp[i].re + far->vEp[i].im * far->vEp[i].im;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vDire[i] * tmp;
			}
		} else if ("LHC" == para.sComponent) {
			for (auto i = 0; i < iNum; ++i) {       // Directivity的左旋分量
				re = far->vEt[i].re - far->vEp[i].im;
				im = far->vEt[i].im + far->vEp[i].re;
				tmp = (re * re + im * im) / 2.0;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vDire[i] * tmp;
			}
		} else {
			for (auto i = 0; i < iNum; ++i) {       // Directivity的右旋分量
				re = far->vEt[i].re + far->vEp[i].im;
				im = far->vEt[i].im - far->vEp[i].re;
				tmp = (re * re + im * im) / 2.0;
				tmp /= (far->vAbsE[i] * far->vAbsE[i]);
				vValue[i] = far->vDire[i] * tmp;
			}
		}
	} else {
		for (auto i = 0; i < iNum; ++i) {           // calculate axial ratio
			vValue[i] = fn_GetAxial(far->vEt[i], far->vEp[i]);
		}
	}
}

void PostData::fn_GetNfcValue(bool &bE, QString &sComponent, QString &sPart, DataEH *near, vector<float> &vValue)
{
	float tmp;
	auto iNum = near->vEx.size();

	vector<Complex> *pE;
	vector<Complex> *pH;
	if ("X" == sComponent) {
		pE = &(near->vEx);
		pH = &(near->vHx);
	} else if ("Y" == sComponent) {
		pE = &(near->vEy);
		pH = &(near->vHy);
	} else if ("Z" == sComponent) {
		pE = &(near->vEz);
		pH = &(near->vHz);
	}

	if (bE) {
		if ("Abs" == sComponent){               // 电场模值
			if ("Mag" == sPart) {
				for (auto i = 0; i < iNum; ++i) {
					tmp  = near->vEx[i].re * near->vEx[i].re + near->vEx[i].im * near->vEx[i].im;
					tmp += near->vEy[i].re * near->vEy[i].re + near->vEy[i].im * near->vEy[i].im;
					tmp += near->vEz[i].re * near->vEz[i].re + near->vEz[i].im * near->vEz[i].im;
					vValue.push_back(sqrt(tmp));
				}
			} else {
				for (auto i = 0; i < iNum; ++i) {
					tmp  = near->vEx[i].re * near->vEx[i].re + near->vEy[i].re * near->vEy[i].re;
					tmp += near->vEz[i].re * near->vEz[i].re;
					vValue.push_back(sqrt(tmp));
				}
			}
		} else {
			if ("Real" == sPart) {              // 电场分量实部或虚部
				for (auto i = 0; i < iNum; ++i) vValue.push_back((*pE)[i].re);
			} else if ("Image" == sPart) {      // 电场分量实部或虚部
				for (auto i = 0; i < iNum; ++i) vValue.push_back((*pE)[i].im);
			} else if ("Mag" == sPart) {        // 电场分量模值
				for (auto i = 0; i < iNum; ++i) {
					tmp = (*pE)[i].re * (*pE)[i].re + (*pE)[i].im * (*pE)[i].im;
					vValue.push_back(sqrt(tmp));
				}
			} else if ("Phase" == sPart) {      // 电场分量相位
				for (auto i = 0; i < iNum; ++i) {
					tmp = fn_GetPhase((*pE)[i].re, (*pE)[i].im);
					vValue.push_back(sqrt(tmp));
				}
			} else {
				for (auto i = 0; i < iNum; ++i) vValue.push_back(fabs((*pE)[i].re));
			}
		}
	} else {
		if ("Abs" == sComponent) {               // 磁场模值
			if ("Mag" == sPart) {
				for (auto i = 0; i < iNum; ++i) {
					tmp  = near->vHx[i].re * near->vHx[i].re + near->vHx[i].im * near->vHx[i].im;
					tmp += near->vHy[i].re * near->vHy[i].re + near->vHy[i].im * near->vHy[i].im;
					tmp += near->vHz[i].re * near->vHz[i].re + near->vHz[i].im * near->vHz[i].im;
					vValue.push_back(sqrt(tmp));
				}
			} else {
				for (auto i = 0; i < iNum; ++i) {
					tmp  = near->vHx[i].re * near->vHx[i].re + near->vHy[i].re * near->vHy[i].re;
					tmp += near->vHz[i].re * near->vHz[i].re;
					vValue.push_back(sqrt(tmp));
				}
			}
		} else {
			if ("Real" == sPart) {              // 磁场分量实部或虚部
				for (auto i = 0; i < iNum; ++i) vValue.push_back((*pH)[i].re);
			} else if ("Image" == sPart) {        // 磁场分量实部或虚部
				for (auto i = 0; i < iNum; ++i) vValue.push_back((*pH)[i].im);
			} else if ("Mag" == sPart) {          // 磁场分量模值
				for (auto i = 0; i < iNum; ++i) {
					tmp = (*pH)[i].re * (*pH)[i].re + (*pH)[i].im * (*pH)[i].im;
					vValue.push_back(sqrt(tmp));
				}
			} else if ("Phase" == sPart) {        // 磁场分量相位
				for (auto i = 0; i < iNum; ++i) {
					tmp = fn_GetPhase((*pH)[i].re, (*pH)[i].im);
					vValue.push_back(sqrt(tmp));
				}
			} else {
				for (auto i = 0; i < iNum; ++i) vValue.push_back(fabs((*pH)[i].re));
			}
		}
	}
}

void PostData::fn_GetCurveFar(FarSet &para, DataFar *far, float **pVal)
{
	int iHead, iStep, iTail;
	if (para.bTheta) {
		iHead = m_vFarPara[para.iFarIndx].vTheta.size();
		iStep = 1;
		iTail = iHead;
	} else {
		iHead = 1;
		iStep = m_vFarPara[para.iFarIndx].vTheta.size();
		iTail = m_vFarPara[para.iFarIndx].vPhi.size();
	}
	float re, im, tmp;
	long indx;
	int j;
	int iNumCurve = para.vAngleIndx.size();
	if ("Electric Field" == para.sQuantity) {
		if ("Abs" == para.sComponent) {          // 电场模值
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					pVal[i][j] = far->vAbsE[indx];
					indx += iStep;
				}
			}
		} else if ("Theta" == para.sComponent) {
			if ("Real" == para.sPart) {           // 电场theta分量实部
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						pVal[i][j] = far->vEt[indx].re;
						indx += iStep;
					}
				}
			} else if ("Image" == para.sPart) {      // 电场theta分量虚部
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						pVal[i][j] = far->vEt[indx].im;
						indx += iStep;
					}
				}
			} else if ("Mag" == para.sPart) {      // 电场theta分量模值
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						tmp = far->vEt[indx].re * far->vEt[indx].re + far->vEt[indx].im * far->vEt[indx].im;
						pVal[i][j] = sqrt(tmp);
						indx += iStep;
					}
				}
			} else {                           // 电场theta分量相位
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						pVal[i][j] = fn_GetPhase(far->vEt[indx].re, far->vEt[indx].im);
						indx += iStep;
					}
				}
			}
		} else {
			if ("Real" == para.sPart) {           // 电场phi分量实部
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						pVal[i][j] = far->vEp[indx].re;
						indx += iStep;
					}
				}
			} else if ("Image" == para.sPart) {      // 电场phi分量虚部
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						pVal[i][j] = far->vEp[indx].im;
						indx += iStep;
					}
				}
			} else if ("Mag" == para.sPart) {      // 电场phi分量模值
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						tmp = far->vEp[indx].re * far->vEp[indx].re + far->vEp[indx].im * far->vEp[indx].im;
						pVal[i][j] = sqrt(tmp);
						indx += iStep;
					}
				}
			} else {                          // 电场phi分量相位
				for (auto i = 0; i < iNumCurve; ++i) {
					indx = para.vAngleIndx[i] * iHead;
					for (j = 0; j < iTail; ++j) {
						pVal[i][j] = fn_GetPhase(far->vEp[indx].re, far->vEp[indx].im);
						indx += iStep;
					}
				}
			}
		}
	} else if ("RCS" == para.sQuantity || "Gain" == para.sQuantity) {
		if ("Abs" == para.sComponent) {           // RCS or Gain mag
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					pVal[i][j] = far->vGain[indx];
					indx += iStep;
				}
			}
		} else if ("Theta" == para.sComponent) {  // RCS or Gain theta
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					tmp = far->vEt[indx].re * far->vEt[indx].re + far->vEt[indx].im * far->vEt[indx].im;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vGain[indx] * tmp;
					indx += iStep;
				}
			}
		} else if ("Phi" == para.sComponent) {    // RCS or Gain phi
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					tmp = far->vEp[indx].re * far->vEp[indx].re + far->vEp[indx].im * far->vEp[indx].im;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vGain[indx] * tmp;
					indx += iStep;
				}
			}
		} else if ("LHC" == para.sComponent) {   // RCS or Gain LHC
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					re = far->vEt[indx].re - far->vEp[indx].im;
					im = far->vEt[indx].im + far->vEp[indx].re;
					tmp = (re * re + im * im) / 2.0;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vGain[indx] * tmp;
					indx += iStep;
				}
			}
		} else {                                 // RCS or Gain RHC
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					re = far->vEt[indx].re + far->vEp[indx].im;
					im = far->vEt[indx].im - far->vEp[indx].re;
					tmp = (re * re + im * im) / 2.0;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vGain[indx] * tmp;
					indx += iStep;
				}
			}
		}
	} else if ("Directivity" == para.sQuantity) {
		if ("Abs" == para.sComponent) {          // Directivity mag
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					pVal[i][j] = far->vDire[indx];
					indx += iStep;
				}
			}
		} else if ("Theta" == para.sComponent) {  // Directivity theta
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					tmp = far->vEt[indx].re * far->vEt[indx].re + far->vEt[indx].im * far->vEt[indx].im;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vDire[indx] * tmp;
					indx += iStep;
				}
			}
		} else if ("Phi" == para.sComponent) {    // Directivity phi
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					tmp = far->vEp[indx].re * far->vEp[indx].re + far->vEp[indx].im * far->vEp[indx].im;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vDire[indx] * tmp;
					indx += iStep;
				}
			}
		} else if ("LHC" == para.sComponent) {    // Directivity LHC
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					re = far->vEt[indx].re - far->vEp[indx].im;
					im = far->vEt[indx].im + far->vEp[indx].re;
					tmp = (re * re + im * im) / 2.0;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vDire[indx] * tmp;
					indx += iStep;
				}
			}
		} else {                                  // Directivity RHC
			for (auto i = 0; i < iNumCurve; ++i) {
				indx = para.vAngleIndx[i] * iHead;
				for (j = 0; j < iTail; ++j) {
					re = far->vEt[indx].re + far->vEp[indx].im;
					im = far->vEt[indx].im - far->vEp[indx].re;
					tmp = (re * re + im * im) / 2.0;
					tmp /= (far->vAbsE[indx] * far->vAbsE[indx]);
					pVal[i][j] = far->vDire[indx] * tmp;
					indx += iStep;
				}
			}
		}
	} else {                                     // calculate axial ratio
		for (auto i = 0; i < iNumCurve; ++i) {
			indx = para.vAngleIndx[i] * iHead;
			for (j = 0; j < iTail; ++j) {
				pVal[i][j] = fn_GetAxial(far->vEt[indx], far->vEp[indx]);
				indx += iStep;
			}
		}
	}
}

void PostData::fn_SetNearMesh(PreNearSet &near, vtkIdType &n, vtkPoints *pnt, vtkCellArray *line, vtkCellArray *poly)
{
	vtkIdType m = n;
	// create points
	int iNumX = 1, iNumY = 1, iNumZ = 1;
	if (fabs(near.dX) > 1.0e-8) iNumX = (near.dMaxX - near.dMinX) / near.dX + 1;
	if (fabs(near.dY) > 1.0e-8) iNumY = (near.dMaxY - near.dMinY) / near.dY + 1;
	if (fabs(near.dZ) > 1.0e-8) iNumZ = (near.dMaxZ - near.dMinZ) / near.dZ + 1;
	Node r;
	for (int i = 0; i < iNumZ; ++i) {
		r.z = near.dMinZ + i * near.dZ;
		for (int j = 0; j < iNumY; ++j) {
			r.y = near.dMinY + j * near.dY;
			for (int k = 0; k < iNumX; ++k) {
				r.x = near.dMinX + k * near.dX;
				pnt->InsertPoint(n, r.x, r.y, r.z);
				++n;
			}
		}
	}
	int num = 0;
	if (iNumX == 1) ++num;
	if (iNumY == 1) ++num;
	if (iNumZ == 1) ++num;
	if (num > 1) {
		// create line
		num = max(iNumX, iNumY);
		num = max(iNumZ, num);
		for (auto i = 1; i < num; ++i) {
			auto seg = vtkLine::New();
			seg->GetPointIds()->SetId(0, m + i - 1);
			seg->GetPointIds()->SetId(1, m + i);
			line->InsertNextCell(seg);
		}
		return;
	}
	vtkIdType v[4];
	vtkIdType iHead;
	vtkIdType iLay = m;
	// XOY
	for (auto k = 0; k < iNumZ; ++k) {
		for (auto i = 1; i < iNumY; ++i) {
			iHead = i * iNumX + iLay;
			for (auto j = 1; j < iNumX; ++j) {
				v[0] = iHead - iNumX + j - 1;
				v[1] = iHead - iNumX + j;
				v[2] = iHead + j;
				v[3] = iHead + j - 1;
				poly->InsertNextCell(4, v);
			}
		}
		iLay += (iNumX * iNumY);
	}
	// XOZ
	iHead = iNumX * iNumY;
	for (auto k = 0; k < iNumY; ++k) {
		iLay = m + k * iNumX;
		for (auto i = 1; i < iNumZ; ++i) {
			for (auto j = 1; j < iNumX; ++j) {
				v[0] = (i - 1) * iHead + iLay + j - 1;
				v[1] = (i - 1) * iHead + iLay + j;
				v[2] = i * iHead + iLay + j;
				v[3] = i * iHead + iLay + j - 1;
				poly->InsertNextCell(4, v);
			}
		}
	}
	// YOZ
	for (auto k = 0; k < iNumX; ++k) {
		iLay = m + k;
		for (auto i = 1; i < iNumZ; ++i) {
			for (auto j = 1; j < iNumY; ++j) {
				v[0] = (i - 1) * iHead + (j - 1) * iNumX + iLay;
				v[1] = (i - 1) * iHead + j * iNumX + iLay;
				v[2] = i * iHead + j * iNumX + iLay;
				v[3] = i * iHead + (j - 1) * iNumX + iLay;
				poly->InsertNextCell(4, v);
			}
		}
	}
}

float PostData::fn_GetAxial(Complex &Et, Complex &Ep)
{
	float rEm = Et.re + Ep.re;
	float iEm = Et.im - Ep.im;
	float rEp = Et.re - Ep.re;
	float iEp = Et.im + Ep.im;
	float re = rEm * rEp + iEm * iEp;
	float im = iEm * rEp - rEm * iEp;
	float tmp = rEp * rEp + iEp * iEp;
	re /= tmp;
	im /= tmp;
	tmp = fn_GetPhase(re, im);
	tmp *= 0.5;
	tmp = tmp * PI / 180.0;
	re = sin(tmp);
	im = cos(tmp);
	rEm = Et.re * re - Ep.re * im;
	iEm = Et.im * re - Ep.im * im;
	rEp = Et.re * im - Ep.re * re;
	iEp = Et.im * im - Ep.im * re;
	re = rEp * rEp + iEp * iEp;
	im = rEm * rEm + iEm * iEm;
	return sqrt(re / im);
}

float fn_GetPhase(float &re, float &im)
{
	float pha = atan2(im, re);
	pha *= (180.0 / PI);

	return pha;
}
