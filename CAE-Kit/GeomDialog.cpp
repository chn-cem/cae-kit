#include <QtWidgets/QMessageBox>
#include "GeomDialog.h"

GeomDialog::GeomDialog(QWidget *parent)
{
    ui.setupUi(this);
    this->setFixedSize(this->width(), this->height());

	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_UpdateMediID()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

void GeomDialog::fn_SetGeomMedi(std::vector<Material> &vMedi, QTreeWidgetItem *pNode, Geometry *geom)
{
	m_pTreeNode = pNode;
	m_pGeom = geom;
	m_vMediID.clear();
	ui.materialList->clear();
	ui.name->setText(QString::fromStdString(geom->sName));
	int indx = -1;
	for (int i = 0; i < vMedi.size(); ++i) {
		m_vMediID.push_back(vMedi[i].iID);
		ui.materialList->addItem(QString::fromStdString(vMedi[i].sName));
		if (geom->iMediID == vMedi[i].iID) {
			indx = i;
		}
	}
	if (indx != -1) {
		ui.materialList->setCurrentIndex(indx);
	}
	this->show();
}

void GeomDialog::fn_UpdateMediID()
{
	if (ui.name->text().isEmpty()) {
		QMessageBox::information(this, "Warn", "The name of lump port has been exist.");
	} else {
		int i = ui.materialList->currentIndex();
		m_pGeom->iMediID = m_vMediID[i];
		m_pGeom->sName = ui.name->text().toStdString();
		m_pTreeNode->setText(0, ui.name->text());
		this->reject();
	}
}

