#ifndef GEOMETRY_DIALOG_H
#define GEOMETRY_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include "ui_GeomDialog.h"
#include "DataBase.h"

class GeomDialog : public QDialog
{
	Q_OBJECT

public:
	GeomDialog(QWidget *parent = Q_NULLPTR);
	~GeomDialog() {}

	void fn_SetGeomMedi(std::vector<Material> &vMedi, QTreeWidgetItem *pNode, Geometry *geom);

private:
	Ui::GeomDialog ui;
	Geometry* m_pGeom{ nullptr };
	QTreeWidgetItem *m_pTreeNode{ nullptr };
	std::vector<int> m_vMediID;

private slots:
	void fn_UpdateMediID();
};

#endif
