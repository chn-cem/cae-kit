#ifndef VOLTAGE_DIALOG_H
#define VOLTAGE_DIALOG_H

#include <QtGui/QKeyEvent>
#include "ui_VoltDialog.h"
#include "DataBase.h"

class VoltDialog : public QDialog
{
	Q_OBJECT

public:
	VoltDialog(QWidget *parent = Q_NULLPTR);
	~VoltDialog();

	Ui::VoltDialog ui;

	void fn_ShowVoltage(DataBase &data);

private:
	DataBase *m_pData{ nullptr };

private slots:
	void keyPressEvent(QKeyEvent *p);
	void fn_ShowRightMenu(const QPoint &pos);
	void fn_AddVoltage();
	void fn_DelVoltage();
	void fn_SaveVoltage();
};

#endif
