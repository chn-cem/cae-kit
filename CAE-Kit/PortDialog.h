#ifndef WAVEPORT_DIALOG_H
#define WAVEPORT_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_PortDialog.h"
#include "DataBase.h"

class PortDialog : public QDialog
{
	Q_OBJECT

public:
	PortDialog(QWidget *parent= Q_NULLPTR);
	~PortDialog() {}

	Ui::PortDialog ui;

	void fn_AddWavePorts(std::vector<WavePort> &vPort, QTreeWidgetItem *pRoot, std::string sType);
	void fn_EditWavePort(std::vector<WavePort> &vPort, QTreeWidgetItem *pRoot, int &iPort);

private:
	std::string m_sType;
	std::vector<WavePort> *m_vPort{ nullptr };
	QTreeWidgetItem *m_pPortNode{ nullptr };
	int m_iCurPort{ -1 };

	void fn_SetPortValue(WavePort &port);
	bool fn_IsNameExist(const std::string &sName, int iSkip);
	void fn_SetDialByType(const std::string &sType);

private slots:
	void fn_AddPortClose();
	void fn_AddPortApply();
	void fn_EditPortClose();

};

#endif