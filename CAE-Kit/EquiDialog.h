#ifndef EQUI_DIALOG_H
#define EQUI_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_EquiDialog.h"
#include "DataBase.h"

class EquiDialog : public QDialog
{
	Q_OBJECT

public:
	
	EquiDialog(QWidget *parent = Q_NULLPTR);
	~EquiDialog() {}

	Ui::EquiDialog ui;

	void fn_AddEquivSrc(std::vector<EquivSrc> &vEqu, QTreeWidgetItem *pRoot);
	void fn_EditEquivSrc(std::vector<EquivSrc> &vEqu, QTreeWidgetItem *pNode, int &iEqu);

private:
	std::vector<EquivSrc> *m_vEqu{ nullptr };
	QTreeWidgetItem *m_pEquNode{ nullptr };
	int m_iCurEqui{ -1 };

	void fn_SetEquiValue(EquivSrc &equ);
	bool fn_IsNameExist(const std::string &sName, int iSkip);

private slots:
	void fn_AddEquiClose();
	void fn_AddEquiApply();
	void fn_EditEquiClose();
};

#endif