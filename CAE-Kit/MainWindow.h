//=================================================================================//
// Main windows
// Author : Wang Yong
// Date   : 2020.10.28
//=================================================================================//
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMenu>
#include <QtGui/QKeyEvent>
#include <QtCore/QProcess>
#include "QVTKOpenGLNativeWidget.h"

#include "ui_MainWindow.h"

#include <string>
#include "DataBase.h"
#include "PostData.h"
#include "DrawMesh.h"
#include "GeomDialog.h"
#include "MediDialog.h"
#include "WaveDialog.h"
#include "WireDialog.h"
#include "LumpDialog.h"
#include "PortDialog.h"
#include "EquiDialog.h"
#include "SymmDialog.h"
#include "FreqDialog.h"
#include "SolvDialog.h"
#include "FarsDialog.h"
#include "NearDialog.h"
#include "VoltDialog.h"
#include "Mpi_Config.h"
#include "VtkImage3D.h"
#include "VtkGraph2D.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = Q_NULLPTR);
    ~MainWindow();

    void fn_ShowVtkWidget();

private:
    Ui::MainWindowClass ui;
    QVTKOpenGLNativeWidget *m_qvtkMesh{ nullptr };

    std::string m_sProjName;
    std::string m_sProjPath;
    std::string m_sPostFile;

    DataBase m_data;
    PostData m_post;
    DrawMesh m_VtkObj;
    QProcess *m_CemExe{ nullptr };
    std::vector<VtkImage3D*> m_vPhoto3D;
    std::vector<VtkGraph2D*> m_vPhoto2D;

    GeomDialog *m_pGeomDialog{ nullptr };
    MediDialog *m_pMediDialog{ nullptr };
    WaveDialog *m_pWaveDialog{ nullptr };
    WireDialog *m_pWireDialog{ nullptr };
    LumpDialog *m_pLumpDialog{ nullptr };
    PortDialog *m_pPortDialog{ nullptr };
    EquiDialog *m_pEquiDialog{ nullptr };
    SymmDialog *m_pSymmDialog{ nullptr };
    FreqDialog *m_pFreqDialog{ nullptr };
    SolvDialog *m_pSolvDialog{ nullptr };
    FarsDialog *m_pFarsDialog{ nullptr };
    NearDialog *m_pNearDialog{ nullptr };
    VoltDialog *m_pVoltDialog{ nullptr };
    Mpi_Config *m_pMpi_Config{ nullptr };

    void fn_ClearTreeNode(QTreeWidgetItem *p);
    void fn_SetProjTree();
    void fn_WriteProjFile();
    void fn_RemoveWindow(std::string &sName);
    void fn_RemoveGraph2(std::string &sName);
    void fn_RemoveGraph3(std::string &sName);
    QTreeWidgetItem *fn_FindTreeNode(QTreeWidgetItem *pNode, QString sName);
    QString fn_GetGraphName(QString sHead, QTreeWidgetItem *p);
    
private slots:
    // function for menu bar
    void fn_OpenProject();
    void fn_SaveProject();
    void fn_ProjSave_As();
    void fn_ClearProjct();
    void fn_AboutWindow();
    void fn_ShowHideMsh();
    void fn_ShowGridMsh();
    void fn_ShowNodeMsh();
    void fn_FitViewMesh();
    void fn_RunPreCheck();
    void fn_Run_OMP_Exe();
    void fn_Run_MPI_Exe();
    void fn_ShowLogInfo();
    void fn_ConfigMpich();
    // function for right click
    void fn_LoadOutMesh();
    void fn_OutGid_Mesh();
    void fn_ShowRightMenu(const QPoint &pos);
    //void fn_VoltRightMenu(const QPoint &pos);
    void fn_DoubleClicked(QTreeWidgetItem *treeNode, int n);
    void fn_A_LeftClicked(QTreeWidgetItem *treeNode, int n);
    void keyPressEvent(QKeyEvent *key);

    void fn_AddMaterial();
    void fn_AddPlanWave();
    void fn_AddWirePort();
    void fn_AddLumpPort();
    void fn_AddRectPort();
    void fn_AddCircPort();
    void fn_AddCoaxPort();
    void fn_AddEquivSrc();
    void fn_AddFarsSets();
    void fn_AddNearSets();

    void fn_SetPostData();
    void fn_DrawFars3D();
    void fn_DrawFars2D();
    void fn_DrawNear3D();
    void fn_DrawCurrent();
    void fn_DrawNetPara();

    void fn_ExportLine();
    void fn_InsertLine();
};

#endif
