#include "DrawCurDlg.h"

DrawCurDlg::DrawCurDlg(QWidget *parent)
{
	ui.setupUi(this);
	ui.gridHead->removeWidget(ui.field);
	delete ui.field;
	ui.field = nullptr;
	this->adjustSize();
	this->setFixedSize(this->width(), this->height());
	this->setWindowTitle("draw current");
	ui.E->setText("J");
	ui.H->setText("M");
	ui.absE->setText("|J|");
	ui.Ex->setText("Jx");
	ui.Ey->setText("Jy");
	ui.Ez->setText("Jz");

	connect(ui.E, SIGNAL(toggled(bool)), this, SLOT(fn_SelectEorH()));
	connect(ui.H, SIGNAL(toggled(bool)), this, SLOT(fn_SelectEorH()));
	connect(ui.absE, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.Ex,   SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.Ey,   SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.Ez,   SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.mag,  SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.pha,  SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.real, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.imag, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.instantMag, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_GetCurPara()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

DrawCurDlg::~DrawCurDlg()
{
	delete nearIndex;
}

void DrawCurDlg::fn_ShowDialog(PostData &post, CurSet &near)
{
	for (int i = 1; i <= post.m_iNumVolt; ++i) {
		ui.voltIndex->addItem(QString::number(i));
	}
	for (auto &freq : post.m_vFreq) {
		ui.freqIndex->addItem(QString::number(freq));
	}
	near.bCancel = true;
	m_post = &(post);
	m_pCurSet = &(near);

	this->exec();
}

void DrawCurDlg::fn_SelectEorH()
{
	if (ui.E->isChecked()) {
		ui.absE->setText("|J|");
		ui.Ex->setText("Jx");
		ui.Ey->setText("Jy");
		ui.Ez->setText("Jz");
	} else {
		ui.absE->setText("|M|");
		ui.Ex->setText("Mx");
		ui.Ey->setText("My");
		ui.Ez->setText("Mz");
	}
}

void DrawCurDlg::fn_ChangePart()
{
	if (ui.absE->isChecked()) {
		ui.real->setDisabled(true);
		ui.imag->setDisabled(true);
		ui.pha->setDisabled(true);
		ui.mag->setEnabled(true);
		ui.instantMag->setEnabled(true);
		ui.norm->setEnabled(true);
		ui.dB->setEnabled(true);
		if ((!ui.mag->isChecked()) && (!ui.instantMag->isChecked())) {
			ui.instantMag->setChecked(true);
		}
	} else {
		ui.instantMag->setEnabled(true);
		ui.mag->setEnabled(true);
		ui.real->setEnabled(true);
		ui.imag->setEnabled(true);
		ui.pha->setEnabled(true);
		if (ui.mag->isChecked() || ui.instantMag->isChecked()) {
			ui.norm->setEnabled(true);
			ui.dB->setEnabled(true);
		} else {
			ui.norm->setDisabled(true);
			ui.norm->setChecked(false);
			ui.dB->setDisabled(true);
			ui.dB->setChecked(false);
		}
	}
}

void DrawCurDlg::fn_GetCurPara()
{
	m_pCurSet->iVoltIndx = ui.voltIndex->currentIndex();
	m_pCurSet->iFreqIndx = ui.freqIndex->currentIndex();

	if (ui.E->isChecked()) {
		m_pCurSet->bIsJ = true;
	} else {
		m_pCurSet->bIsJ = false;
	}

	if (ui.absE->isChecked()) {
		m_pCurSet->sComponent = "Abs";
	} else if (ui.Ex->isChecked()) {
		m_pCurSet->sComponent = "X";
	} else if (ui.Ey->isChecked()) {
		m_pCurSet->sComponent = "Y";
	} else {
		m_pCurSet->sComponent = "Z";
	}

	if (ui.real->isChecked()) {
		m_pCurSet->sPart = "Real";
	} else if (ui.imag->isChecked()) {
		m_pCurSet->sPart = "Image";
	} else if (ui.mag->isChecked()) {
		m_pCurSet->sPart = "Mag";
	} else if (ui.pha->isChecked()) {
		m_pCurSet->sPart = "Phase";
	} else {
		m_pCurSet->sPart = "Instant";
	}

	if (ui.norm->isChecked()) {
		m_pCurSet->bNormlize = true;
	} else {
		m_pCurSet->bNormlize = false;
	}

	if (ui.dB->isChecked()) {
		m_pCurSet->bSetdB = true;
	} else {
		m_pCurSet->bSetdB = false;
	}

	m_pCurSet->bCancel = false;
	this->reject();
}

