#include <QtWidgets/QMessageBox>
#include "PortDialog.h"

PortDialog::PortDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->setFixedSize( this->width (),this->height ());

	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply, SIGNAL(clicked()), this, SLOT(fn_AddPortApply()));
}

void PortDialog::fn_AddWavePorts(std::vector<WavePort> &vPort, QTreeWidgetItem *pRoot, std::string sType)
{
	setWindowTitle("Add Waveport");
	// set dialog by waveport type
	fn_SetDialByType(sType);

	m_sType = sType;
	m_vPort = &vPort;
	m_pPortNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "Port" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddPortClose()));
	this->show();
}

void PortDialog::fn_EditWavePort(std::vector<WavePort> &vPort, QTreeWidgetItem *pRoot, int &iPort)
{
	setWindowTitle("Edit Waveport");
	// set dialog by waveport type
	fn_SetDialByType(vPort[iPort].sType);
	ui.name->setText(QString::fromStdString(vPort[iPort].sName));
	ui.a->setText(QString::number(vPort[iPort].a));
	ui.b->setText(QString::number(vPort[iPort].b));
	ui.x->setText(QString::number(vPort[iPort].Oc.x));
	ui.y->setText(QString::number(vPort[iPort].Oc.y));
	ui.z->setText(QString::number(vPort[iPort].Oc.z));
	ui.ax->setText(QString::number(vPort[iPort].ai.x));
	ui.ay->setText(QString::number(vPort[iPort].ai.y));
	ui.az->setText(QString::number(vPort[iPort].ai.z));
	ui.ex->setText(QString::number(vPort[iPort].ei.x));
	ui.ey->setText(QString::number(vPort[iPort].ei.y));
	ui.ez->setText(QString::number(vPort[iPort].ei.z));
	ui.apply->setDisabled(true);

	m_sType = vPort[iPort].sType;
	m_vPort = &vPort;
	m_pPortNode = pRoot;
	m_iCurPort = iPort;

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditPortClose()));
	this->show();
}

void PortDialog::fn_AddPortClose()
{
	WavePort port;

	fn_SetPortValue(port);
	if (!fn_IsNameExist(port.sName, -1)) {
		m_vPort->push_back(port);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(port.sName));
		m_pPortNode->addChild(pSon);
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of waveport has been exist.");
	}
}

void PortDialog::fn_AddPortApply()
{
	WavePort port;

	fn_SetPortValue(port);
	if (!fn_IsNameExist(port.sName, -1)) {
		m_vPort->push_back(port);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(port.sName));
		m_pPortNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "Port" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of waveport has been exist.");
	}
}

void PortDialog::fn_EditPortClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCurPort)) {
		fn_SetPortValue((*m_vPort)[m_iCurPort]);
		m_pPortNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of waveport has been exist.");
	}
}

void PortDialog::fn_SetPortValue(WavePort &port)
{
	port.sType = m_sType;
	port.sName = ui.name->text().toStdString();
	port.Oc.x = ui.x->text().toDouble();
	port.Oc.y = ui.y->text().toDouble();
	port.Oc.z = ui.z->text().toDouble();
	port.ei.x = ui.ex->text().toDouble();
	port.ei.y = ui.ey->text().toDouble();
	port.ei.z = ui.ez->text().toDouble();
	port.ai.x = ui.ax->text().toDouble();
	port.ai.y = ui.ay->text().toDouble();
	port.ai.z = ui.az->text().toDouble();
	port.a = ui.a->text().toDouble();
	port.b = ui.b->text().toDouble();
}

bool PortDialog::fn_IsNameExist(const std::string &sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vPort).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vPort)[i].sName == sName) {
			return true;
		}
	}
	return false;
}

void PortDialog::fn_SetDialByType(const std::string& sType)
{
	if ("Rectangle" == sType) {
		ui.widthlab->setText("Width");
		ui.heighlab->setText("Height");
		ui.heighlab->setVisible(true);
		ui.b->setVisible(true);
		ui.ex->setVisible(true);
		ui.ey->setVisible(true);
		ui.ez->setVisible(true);
		ui.ein->setVisible(true);
	}
	else if ("Circular" == sType) {
		ui.widthlab->setText("Radius");
		ui.heighlab->setHidden(true);
		ui.b->setHidden(true);
		ui.ex->setVisible(true);
		ui.ey->setVisible(true);
		ui.ez->setVisible(true);
		ui.ein->setVisible(true);
	}
	else {
		ui.widthlab->setText("Inner Radius");
		ui.heighlab->setText("Outer Radius");
		ui.heighlab->setVisible(true);
		ui.b->setVisible(true);
		ui.ex->setHidden(true);
		ui.ey->setHidden(true);
		ui.ez->setHidden(true);
		ui.ein->setHidden(true);
	}
}

