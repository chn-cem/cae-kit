#include <QtWidgets/QMessageBox>
#include <iostream>
#include <sstream>
#include <fstream>
#include "vtkAxis.h"
#include "VtkGraph2D.h"
#include "vtkContextScene.h"
#include "vtkPlotLine.h"
#include "vtkCamera.h"
#include "vtkGenericOpenGLRenderWindow.h"
#include "vtkAxesActor.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkScalarBarActor.h"
#include "vtkTextProperty.h"
#include "vtkBMPWriter.h"
#include "vtkPNGWriter.h"
#include "vtkJPEGWriter.h"
#include "vtkRenderLargeImage.h"
#include "vtkFloatArray.h"
#include "vtkPen.h"

VtkGraph2D::VtkGraph2D(QString &sName)
{
	m_sName = sName;
	m_tab = new QWidget();
	m_gridLy = new QGridLayout(m_tab);
	m_gridLy->setSpacing(0);
	m_gridLy->setContentsMargins(0, 0, 0, 0);

	m_qvtkWidget = new QVTKOpenGLNativeWidget(m_tab);
	m_gridLy->addWidget(m_qvtkWidget, 0, 0, 1, 1);

	m_table = nullptr;
	m_chart = vtkSmartPointer<vtkChartXY>::New();
	m_view = vtkSmartPointer<vtkContextView>::New();
	
}

VtkGraph2D::~VtkGraph2D()
{
	fn_Clear();
}

void VtkGraph2D::fn_SetName(QString &sName)
{
	m_sName = sName;
}

void VtkGraph2D::fn_SetTable(vtkSmartPointer<vtkTable> table)
{
	m_table = table;
}

QVTKOpenGLNativeWidget* VtkGraph2D::fn_GetVTKWidget()
{
	return m_qvtkWidget;
}

QString VtkGraph2D::fn_GetName()
{
	return m_sName;
}

QWidget* VtkGraph2D::fn_GetTab()
{
	return m_tab;
}

void VtkGraph2D::fn_ToRender(string &xlabel, string &ylabel)
{
	int ic = m_table->GetNumberOfRows();
	int iP = m_table->GetNumberOfColumns();
	m_chart->SetShowLegend(true);                // 显示标签
	m_chart->SetInteractive(true);               // 显示鼠标附近点数值
	m_chart->SetAutoAxes(false);
	m_chart->GetAxis(vtkAxis::BOTTOM)->SetTitle(xlabel);
	m_chart->GetAxis(vtkAxis::LEFT)->SetTitle(ylabel);
	m_chart->GetAxis(vtkAxis::TOP)->SetVisible(true);
	m_chart->GetAxis(vtkAxis::TOP)->SetTicksVisible(true);
	m_chart->GetAxis(vtkAxis::TOP)->SetNumberOfTicks(0);
	m_chart->GetAxis(vtkAxis::RIGHT)->SetVisible(true);
	m_chart->GetAxis(vtkAxis::RIGHT)->SetTicksVisible(true);
	m_chart->GetAxis(vtkAxis::RIGHT)->SetNumberOfTicks(0);
	// Set up a 2D scene, add an XY chart to it
	m_view->GetScene()->AddItem(m_chart);
	m_view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
	
	// add each curve to the widget
	int num = m_table->GetNumberOfRows();
	vtkPlotLine *line;
	vtkPlot *points;
	for (int i = 1; i < m_table->GetNumberOfColumns(); ++i) {
		if (num < 2) {
			points = vtkPlot::SafeDownCast(m_chart->AddPlot(vtkChart::POINTS));
			points->SetInputData(m_table, 0, i);
			points->SetWidth(1.5);
		} else {
			line = vtkPlotLine::SafeDownCast(m_chart->AddPlot(vtkChart::LINE));
			line->SetInputData(m_table, 0, i);
			line->SetWidth(1.5);
		}
		
	}
	// render on the windows
	m_view->SetRenderWindow(m_qvtkWidget->GetRenderWindow());
	m_view->GetRenderWindow()->Render();
}

void VtkGraph2D::fn_SaveLine(string sName )
{
	ofstream fp(sName);

	if(!fp.is_open()) {
		QMessageBox::information(nullptr,"Warn","Can't save the line data to file.");
		return;
	}
	int n = m_table->GetNumberOfColumns();
	int m = m_table->GetNumberOfRows();
	fp << m << " " << n << endl;
	for(auto i = 0; i < m; ++i) {
		for(auto j = 0; j < n; ++j) {
			fp << m_table->GetValue(i, j)<<" ";
		}
		fp << endl;
	}
	fp.close();
}

void VtkGraph2D::fn_SaveGraph(string &sName)
{
	vtkRenderLargeImage *image = vtkRenderLargeImage::New();
	image->SetInput(m_view->GetRenderer());
	image->SetMagnification(1);

	int len = sName.length();
	std::string kind = sName.substr(len - 4);
	if (kind == ".bmp") {
		vtkBMPWriter *picOut = vtkBMPWriter::New();
		picOut->SetInputConnection(image->GetOutputPort());
		picOut->SetFileName(sName.c_str());
		picOut->Write();
		picOut->Delete();
	} else if (kind == ".png") {
		vtkPNGWriter *picOut = vtkPNGWriter::New();
		picOut->SetInputConnection(image->GetOutputPort());
		picOut->SetFileName(sName.c_str());
		picOut->Write();
		picOut->Delete();
	} else if (kind == ".jpg") {
		vtkJPEGWriter *picOut = vtkJPEGWriter::New();
		picOut->SetInputConnection(image->GetOutputPort());
		picOut->SetFileName(sName.c_str());
		picOut->Write();
		picOut->Delete();
	}

	image->Delete();
}

void VtkGraph2D::fn_AddLine(string sName)
{
	ifstream fp(sName);
	if(!fp.is_open()){
		QMessageBox::information(NULL, "Warn", "Can't open the data file !");
		return;
	}

	int i, j;
	int num_point, num_curve;
	fp >> num_point >> num_curve;

	// Create a table with some curves in it
	vtkTable *table = vtkTable::New();

	// 添加X轴标注
	auto arrX = vtkSmartPointer<vtkFloatArray>::New();
	arrX->SetName("");
	table->AddColumn(arrX);

	// 添加Y轴标注
	ostringstream int_str;
	string legend;
	for (i = 1; i < num_curve; ++i) {
		auto arrY = vtkSmartPointer<vtkFloatArray>::New();
		int_str.str("");
		int_str << (i + 1);
		legend = "External_" + int_str.str();
		arrY->SetName(legend.c_str());
		table->AddColumn(arrY);
	}
	table->SetNumberOfRows(num_point);

	// add value
	float val;
	for(i = 0; i < num_point; ++i) {
		for(j = 0; j < num_curve; ++j) {
			fp >> val;
			table->SetValue( i, j, val );
		}
	}
	fp.close();

	// add each curve to the widget
	vtkPlotLine *line;
	vtkPlot *points;
	vtkPen *pen = vtkPen::New();
	pen->SetLineType(vtkPen::DASH_LINE);
	for(i = 1; i < table->GetNumberOfColumns(); ++i) {
		val = i / 2.55;
		if( num_point < 2 ) {
			points = vtkPlot::SafeDownCast(m_chart->AddPlot(vtkChart::POINTS));
			points->SetInputData(table, 0, i);
			points->SetWidth(1.5);
			line->SetColor(val, val, val);
		} else {
			line = vtkPlotLine::SafeDownCast(m_chart->AddPlot(vtkChart::LINE));
			line->SetInputData(table, 0, i);
			line->SetWidth(1.5);
			line->SetPen(pen);
			line->SetColor(val, val, val);
		}
	}

	table->Delete();
	pen->Delete();
}

void VtkGraph2D::fn_Clear()
{
	m_qvtkWidget->GetRenderWindow()->Finalize();
	m_qvtkWidget->GetRenderWindow()->FastDelete();

	m_table->Delete();
	m_chart->Delete();

	// the order of following sentence is very important
	if (nullptr != m_qvtkWidget) delete m_qvtkWidget;
	if (nullptr != m_gridLy) delete m_gridLy;
	if (nullptr != m_tab) delete m_tab;
    m_tab = nullptr;
	m_gridLy = nullptr;
	m_qvtkWidget = nullptr;
}
