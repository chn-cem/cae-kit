#include <QtWidgets/QListWidgetItem>
#include <QtWidgets/QMessageBox>
#include <fstream>
#include <sstream>
#include <string>
#include "DrawNetDlg.h"
#include "vtkFloatArray.h"
#include "vtkSmartPointer.h"

DrawNetDlg::DrawNetDlg(QWidget *parent, int iNumPort)
{
	ui.setupUi(this);
	this->setFixedSize( this->width (),this->height ());

	m_iNumPort = iNumPort;

	int n = 0;
	for (auto i = 1; i <= m_iNumPort; ++i) {
		for (auto j = 1; j <= m_iNumPort; ++j) {
			QString text = "S(" + QString::number(i) + "," + QString::number(j) + ")";
			QListWidgetItem *plist = new QListWidgetItem(ui.source);
			plist->setText(text);
			ui.source->insertItem(n, plist);
			++n;
		}
	}

	connect(ui.netType, SIGNAL(currentIndexChanged(int)), this, SLOT(fn_ChangePara()));
	connect(ui.mag, SIGNAL(clicked()), this, SLOT(fn_ChangePart()));
	connect(ui.pha, SIGNAL(clicked()), this, SLOT(fn_ChangePart()));
	connect(ui.real, SIGNAL(clicked()), this, SLOT(fn_ChangePart()));
	connect(ui.imag, SIGNAL(clicked()), this, SLOT(fn_ChangePart()));
	connect(ui.insert, SIGNAL(clicked()), this, SLOT(fn_InsertSelect()));
	connect(ui.remove, SIGNAL(clicked()), this, SLOT(fn_RemoveSelect()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_GetNetPara()));
}

void DrawNetDlg::fn_ChangePara()
{
	ui.source->clear();
	ui.select->clear();
	QString sType = ui.netType->currentText();

	int i, j;
	QString text;
	int n = 0;
	if (sType != "SWR") {
		for (auto i = 1; i <= m_iNumPort; ++i) {
			for (auto j = 1; j <= m_iNumPort; ++j) {
				text = sType + "(" + QString::number(j) + "," + QString::number(i) + ")";
				QListWidgetItem* plist = new QListWidgetItem(ui.source);
				plist->setText(text);
				ui.source->insertItem(n, plist);
				++n;
			}
		}
		ui.real->setEnabled(true);
		ui.imag->setEnabled(true);
		ui.mag->setEnabled(true);
		ui.pha->setEnabled(true);
		ui.mag->setChecked(true);
		ui.dB->setEnabled(true);
		ui.dB->setChecked(true);
	} else {
		for (i = 1; i <= m_iNumPort; ++i) {
			text = "SWR(" + QString::number(i) + ")";
			QListWidgetItem* plist = new QListWidgetItem(ui.source);
			plist->setText(text);
			ui.source->insertItem(n, plist);
			++n;
		}
		ui.real->setDisabled(true);
		ui.imag->setDisabled(true);
		ui.pha->setDisabled(true);
		ui.mag->setEnabled(true);
		ui.mag->setChecked(true);
		ui.dB->setDisabled(true);
		ui.dB->setChecked(false);
	}
}

void DrawNetDlg::fn_ChangePart()
{
	if (ui.mag->isChecked() && ui.netType->currentText() != "SWR") {
		ui.dB->setEnabled(true);
	} else {
		ui.dB->setDisabled(true);
		ui.dB->setChecked(false);
	}
}

void DrawNetDlg::fn_InsertSelect()
{
	QString sHead;
	if (ui.dB->isChecked()) {
		sHead = "dB[";
	} else {
		if (ui.real->isChecked()) {
			sHead = "Re[";
		} else if (ui.imag->isChecked()) {
			sHead = "Im[";
		} else if (ui.mag->isChecked()) {
			sHead = "Mag[";
		} else {
			sHead = "Phase[";
		}
	}

	QString sLabel;
	int n = 0;
	for(auto i = 0; i < ui.source->count(); ++i) {
		if (ui.source->item(i)->isSelected()) {
			if (ui.netType->currentText() == "SWR") {
				sLabel = ui.source->item(i)->text();
			} else {
				sLabel = sHead + ui.source->item(i)->text() + "]";
			}
			bool flag = true;
			for(auto j = 0; j < ui.select->count(); ++j) {
				if( ui.select->item(j)->text() == sLabel) {
					flag = false;
					break;
				}
			}
			if( flag ) {
				QListWidgetItem *plist = new QListWidgetItem(ui.select);
				plist->setText(sLabel);
				ui.select->insertItem(n, plist);
				++n;
			}
		}
	}
}

void DrawNetDlg::fn_RemoveSelect()
{
	std::vector<int> vDelIndx;
	for (int i = 0; i < ui.select->count(); ++i) {
		if (ui.select->item(i)->isSelected()) {
			vDelIndx.push_back(i);
		}
	}
	for (int i = vDelIndx.size() - 1; i >= 0; --i) {
		ui.select->takeItem(vDelIndx[i]);
	}
	vDelIndx.clear();
}

void DrawNetDlg::fn_ShowDialog(NetSet &para)
{
	m_pNetSet = &(para);
	m_pNetSet->bCancel = true;
	this->exec();
}

void DrawNetDlg::fn_GetNetPara()
{
	m_pNetSet->sType = ui.netType->currentText().toStdString();
	for (auto i = 0; i < ui.select->count(); ++i) {
		m_pNetSet->vLabel.push_back(ui.select->item(i)->text().toStdString());
	}
	m_pNetSet->vPart.resize(m_pNetSet->vLabel.size());

	int iHead, iMidd, iTail, m, n;
	for (auto i = 0; i < m_pNetSet->vLabel.size(); ++i) {
		iHead = m_pNetSet->vLabel[i].find("(") + 1;
		iMidd = m_pNetSet->vLabel[i].find(",") + 1;
		iTail = m_pNetSet->vLabel[i].find(")") + 1;
		m = iMidd - iHead - 1;
		n = iTail - iMidd - 1;
		if (m_pNetSet->sType == "SWR") {
			m = iTail - iHead - 1;
			n = atoi(m_pNetSet->vLabel[i].substr(iHead, m).c_str()) - 1;
			m_pNetSet->vPortI.push_back(n);
		} else {
			m = atoi(m_pNetSet->vLabel[i].substr(iHead, m).c_str()) - 1;
			n = atoi(m_pNetSet->vLabel[i].substr(iMidd, n).c_str()) - 1;
			m_pNetSet->vPortI.push_back(m);
			m_pNetSet->vPortJ.push_back(n);
		}
		if (m_pNetSet->vLabel[i].find("Re") != std::string::npos) {
			m_pNetSet->vPart[i] = 0;
		} else if (m_pNetSet->vLabel[i].find("Im") != std::string::npos) {
			m_pNetSet->vPart[i] = 1;
		} else if (m_pNetSet->vLabel[i].find("Mag") != std::string::npos) {
			m_pNetSet->vPart[i] = 2;
		} else if (m_pNetSet->vLabel[i].find("dB") != std::string::npos) {
			m_pNetSet->vPart[i] = 3;
		} else if (m_pNetSet->vLabel[i].find("Phase") != std::string::npos) {
			m_pNetSet->vPart[i] = 4;
		} else {
			m_pNetSet->vPart[i] = 5;
		}
	}
	m_pNetSet->bCancel = false;
	this->reject();
}

vtkTable* DrawNetDlg::fn_CreateTable(std::string &fname, bool &ok )
{
	ok = false;
	std::ifstream fp;
	fp.open(fname);
	if(!fp.is_open()) {
		QMessageBox::information(this, "Warning", "Can't load the network parameter file.");
		return nullptr;
	}

	QString kind = ui.netType->currentText();
	int num_freq, num_port;
	int i, j;
	std::string syz;

	fp>>syz>>num_freq>>num_port;
	float *freq = new float[num_freq];
	float **Re = new float*[num_freq];
	float **Im = new float*[num_freq];
	int num = num_port * num_port;
	for( i = 0; i < num_freq; i++ ) {
		Re[i] = new float[num];
		Im[i] = new float[num];
	}
	// 读入S参数
	for( i = 0; i < num_freq; i++ ) {
		fp>>freq[i];
		for( j = 0; j < num; j++ )
		{
			fp>>Re[i][j]>>Im[i][j];
		}
	}
	// 读入Z参数
	if( "S" != kind && "SWR" != kind ) {
		fp >> syz >> num_freq >> num_port;
		for (i = 0; i < num_freq; i++) {
			fp >> freq[i];
			for (j = 0; j < num; j++)
			{
				fp >> Re[i][j] >> Im[i][j];
			}
		}
		// 读入Y参数
		if( "Z" != kind ) {
			fp >> syz >> num_freq >> num_port;
			for (i = 0; i < num_freq; i++)
			{
				fp >> freq[i];
				for (j = 0; j < num; j++)
				{
					fp >> Re[i][j] >> Im[i][j];
				}
			}
		}
	}
	fp.close();

	std::vector<QString> item;

	for( i = 0; i < ui.select->count(); i++ ) {
		item.push_back(ui.select->item(i)->text());
	}
	// Create a table with some curves in it
	vtkTable *table = vtkTable::New();
	// 获取需要绘制曲线条数
	int num_curve = item.size();

	// 添加Y轴标注
	for( i = 0; i < num_curve; i++ ) {
		auto arrY = vtkSmartPointer<vtkFloatArray>::New();
		arrY->SetName( item[i].toStdString().c_str() );
		table->AddColumn( arrY );
	}
	// 设置每条曲线数据点数
	table->SetNumberOfRows( num_freq );
	// add value of X-axis
	for( i = 0; i < num_freq; i++ ) {
		table->SetValue( i, 0, freq[i] );
	}
	delete [] freq;

	int m, n, k;
	float mag;
	QString indx;
	//for( i = 0; i < num_curve; i++ ) {
	//	if ("Real" == item[i])          // Real
	//	{
	//		j = item[i].length() - 7;
	//		indx = item[i].mid( 5, j );
	//		m = indx.section(',', 0, 0).toInt();
	//		n = indx.section(',', 1, 1).toInt();
	//		k = (n - 1) * num_port + m-1;
	//		for( j = 0; j < num_freq; j++ )
	//		{
	//			table->SetValue(j, i + 1, Re[j][k] );
	//		}
	//	}else if ("Image" == item[i])    // Imag
	//	{
	//		j = item[i].length() - 7;
	//		indx = item[i].mid(5, j);
	//		m = indx.section(',', 0, 0).toInt();
	//		n = indx.section(',', 1, 1).toInt();
	//		k = (n - 1) * num_port + m-1;
	//		for (j = 0; j < num_freq; j++)
	//		{
	//			table->SetValue(j, i + 1, Im[j][k]);
	//		}
	//	}else if ("Mag" == item[i])    // Mag
	//	{
	//		j = item[i].length() - 5;
	//		indx = item[i].mid(4, j);
	//		if (indx.indexOf("SWR") > -1)
	//		{
	//			j = indx.length() - 5;
	//			indx = indx.mid(4, j);
	//			m = indx.toInt();
	//			n = m;
	//			k = (n - 1) * num_port + m-1;
	//			for (j = 0; j < num_freq; j++)
	//			{
	//				mag = Re[j][k] * Re[j][k] + Im[j][k] * Im[j][k];
	//				mag = sqrt(mag);
	//				cent = (1.0 + mag) / (1.0 - mag);
	//				table->SetValue(j, i + 1, cent);
	//			}
	//		}else
	//		{
	//			j = indx.length() - 3;
	//			indx = indx.mid(2, j);
	//			m = indx.section(',', 0, 0).toInt();
	//			n = indx.section(',', 1, 1).toInt();
	//			k = (n - 1) * num_port + m-1;
	//			for (j = 0; j < num_freq; j++)
	//			{
	//				mag = Re[j][k] * Re[j][k] + Im[j][k] * Im[j][k];
	//				mag = sqrtf(mag);
	//				table->SetValue(j, i + 1, mag);
	//			}
	//		}
	//	}else if ("Phase" == item[i])    // Phase
	//	{
	//		j = item[i].length() - 10;
	//		indx = item[i].mid(8, j);
	//		m = indx.section(',', 0, 0).toInt();
	//		n = indx.section(',', 1, 1).toInt();
	//		k = (n - 1) * num_port + m-1;
	//		for (j = 0; j < num_freq; j++)
	//		{
	//			table->SetValue(j, i + 1, fn_GetPhase(Re[j][k], Im[j][k]));
	//		}
	//	}else                              // dB
	//	{
	//		j = item[i].length() - 7;
	//		indx = item[i].mid(5, j);
	//		m = indx.section(',', 0, 0).toInt();
	//		n = indx.section(',', 1, 1).toInt();
	//		k = (n - 1) * num_port + m-1;
	//		for (j = 0; j < num_freq; j++)
	//		{
	//			mag = Re[j][k] * Re[j][k] + Im[j][k] * Im[j][k];
	//			cent = 10.0*log10(mag);
	//			table->SetValue(j, i + 1, cent);
	//		}
	//	}
	//}

	// 清理内存
	for (i = 0; i < num_freq; i++) {
		delete [] Re[i], Im[i];
	}
	delete[] Re, Im;

	ok = true;
	return table;
}
