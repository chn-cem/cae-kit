#ifndef MATERIALDIALOG_H
#define MATERIALDIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_MediDialog.h"
#include "DataBase.h"

class MediDialog : public QDialog
{
	Q_OBJECT

public:
	MediDialog(QWidget *parent= Q_NULLPTR);
	~MediDialog() {}

	Ui::MediDialog ui;

	void fn_AddMaterial(std::vector<Material> &vMedi, QTreeWidgetItem *pRoot);
	void fn_EditMaterial(std::vector<Material> &vMedie, QTreeWidgetItem *pNode, int &iMedi);

private:
	std::vector<Material> *m_vMedi{ nullptr };
	QTreeWidgetItem *m_pMediNode{ nullptr };
	int m_iCur{ -1 };

	void fn_SetMediValue(Material &medi);
	bool fn_IsNameExist(const std::string &sName, int iSkip);

private slots:
	void fn_MediLossType();
	void fn_AddMediClose();
	void fn_AddMediApply();
	void fn_EditMediClose();
};

#endif