#include <QtWidgets/QMessageBox>
#include "LumpDialog.h"

LumpDialog::LumpDialog(QWidget *parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->width(), this->height());

	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply,  SIGNAL(clicked()), this, SLOT(fn_AddLumpApply()));
}

void LumpDialog::fn_AddLumpPorts(std::vector<LumpPort> &vLump, QTreeWidgetItem *pRoot)
{
	setWindowTitle("Add Lump Port");

	m_vLump = &vLump;
	m_pLumpNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "Lump" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddLumpClose()));
	this->show();
}

void LumpDialog::fn_EditLumpPort(std::vector<LumpPort> &vLump, QTreeWidgetItem *pNode, int &iLump)
{
	setWindowTitle("Edit Lump Port");
	ui.name->setText(QString::fromStdString(vLump[iLump].sName));
	ui.x->setText(QString::number(vLump[iLump].Oc.x));
	ui.y->setText(QString::number(vLump[iLump].Oc.y));
	ui.z->setText(QString::number(vLump[iLump].Oc.z));
	ui.ex->setText(QString::number(vLump[iLump].ei.x));
	ui.ey->setText(QString::number(vLump[iLump].ei.y));
	ui.ez->setText(QString::number(vLump[iLump].ei.z));
	ui.nx->setText(QString::number(vLump[iLump].norm.x));
	ui.ny->setText(QString::number(vLump[iLump].norm.y));
	ui.nz->setText(QString::number(vLump[iLump].norm.z));
	ui.width->setText(QString::number(vLump[iLump].a));
	ui.heigh->setText(QString::number(vLump[iLump].b));
	ui.apply->setDisabled(true);

	m_vLump = &vLump;
	m_pLumpNode = pNode;
	m_iCurLump = iLump;

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditLumpClose()));
	this->show();
}

void LumpDialog::fn_AddLumpClose()
{
	LumpPort Lump;

	fn_SetLumpValue(Lump);
	if (!fn_IsNameExist(Lump.sName, -1)) {
		m_vLump->push_back(Lump);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(Lump.sName));
		m_pLumpNode->addChild(pSon);
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of lump port has been exist.");
	}
}

void LumpDialog::fn_AddLumpApply()
{
	LumpPort Lump;

	fn_SetLumpValue(Lump);
	if (!fn_IsNameExist(Lump.sName, -1)) {
		m_vLump->push_back(Lump);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(Lump.sName));
		m_pLumpNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "Lump" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of lump port has been exist.");
	}
}

void LumpDialog::fn_EditLumpClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCurLump)) {
		fn_SetLumpValue((*m_vLump)[m_iCurLump]);
		m_pLumpNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of lump port has been exist.");
	}
}

void LumpDialog::fn_SetLumpValue(LumpPort &Lump)
{
	Lump.sName = ui.name->text().toStdString();
	Lump.Oc.x = ui.x->text().toDouble();
	Lump.Oc.y = ui.y->text().toDouble();
	Lump.Oc.z = ui.z->text().toDouble();
	Lump.ei.x = ui.ex->text().toDouble();
	Lump.ei.y = ui.ey->text().toDouble();
	Lump.ei.z = ui.ez->text().toDouble();
	Lump.norm.x = ui.nx->text().toDouble();
	Lump.norm.y = ui.ny->text().toDouble();
	Lump.norm.z = ui.nz->text().toDouble();
	Lump.a = ui.width->text().toDouble();
	Lump.b = ui.heigh->text().toDouble();
}

bool LumpDialog::fn_IsNameExist(const std::string &sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vLump).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vLump)[i].sName == sName) {
			return true;
		}
	}
	return false;
}
