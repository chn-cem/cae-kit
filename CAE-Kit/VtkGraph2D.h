#ifndef VTK_GRAPH_2D_H
#define VTK_GRAPH_2D_H

#include <QtWidgets/QGridLayout>

#include "QVTKOpenGLNativeWidget.h"
#include "vtkSmartPointer.h"
#include "vtkContextView.h"
#include "vtkChartXY.h"
#include "vtkTable.h"
#include "vtkActor.h"
#include "vtkRenderer.h"

using namespace std;

class VtkGraph2D
{
public:
	VtkGraph2D(QString &sName);
	~VtkGraph2D();

private:
	QString m_sName;
	QWidget *m_tab{ nullptr };
	QGridLayout *m_gridLy{ nullptr };
	QVTKOpenGLNativeWidget *m_qvtkWidget{ nullptr };
	vtkSmartPointer<vtkChartXY> m_chart{ nullptr };
	vtkSmartPointer<vtkContextView> m_view{ nullptr };
	vtkSmartPointer<vtkTable> m_table{ nullptr };

public:
	void fn_SetName(QString &sName);
	void fn_SetTable(vtkSmartPointer<vtkTable> table);
	void fn_ToRender(string &xlabel, string &ylabel);
	void fn_SaveLine(string fname);
	void fn_SaveGraph(string &fname);
	void fn_AddLine(string fname);
	void fn_Clear();

	QString fn_GetName();
	QWidget *fn_GetTab();
	QVTKOpenGLNativeWidget* fn_GetVTKWidget();
};

#endif