#ifndef DRAW_MESH_H
#define DRAW_MESH_H

#include <vector>
#include "vtkSmartPointer.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"
#include "QVTKOpenGLNativeWidget.h"
#include "Node.h"
#include "DataBase.h"

class DrawMesh
{
public:
	DrawMesh();
	~DrawMesh();

	vtkSmartPointer<vtkPolyData> m_gdmObj{ nullptr };
	vtkSmartPointer<vtkUnstructuredGrid> m_tetObj{ nullptr };
	vtkSmartPointer<vtkActor> m_actor{ nullptr };
	vtkSmartPointer<vtkRenderer> m_render{ nullptr };

	Node m_Oc;
	double m_radius{ 0 };
	bool m_bShow{ true };

public:
	void fn_CreateMap(FaceMesh &gdm);
	void fn_CreateMap(VoluMesh &tet);
	void fn_RenderWin(QVTKOpenGLNativeWidget *qvtkWidget);
	void fn_UpdateMsh(QVTKOpenGLNativeWidget *qvtkWidget);
	void fn_ClearMesh(QVTKOpenGLNativeWidget *qvtkWidget);
	void fn_ShowHideMsh();
	void fn_ShowGridMsh();
	void fn_ShowNodeMsh();
	vtkSmartPointer<vtkPolyData> fn_GetGdmMesh();
	vtkSmartPointer<vtkUnstructuredGrid> fn_GetTetMesh();

private:
	void fn_ClearActor();
};

#endif