#ifndef FREQUENCY_DIALOG_H
#define FREQUENCY_DIALOG_H

#include "ui_FreqDialog.h"
#include "DataBase.h"

class FreqDialog : public QDialog
{
	Q_OBJECT

public:
	FreqDialog(QWidget *parent = Q_NULLPTR);
	~FreqDialog() {}

	void fn_SetFrequency(FreqPara &freqSet);

private:
	Ui::FreqDialog ui;
	FreqPara *m_pFreqSet{ nullptr };

private slots:
	void fn_CheckUnit(QString sUnit);
	void fn_UpdateFreq();
};

#endif
