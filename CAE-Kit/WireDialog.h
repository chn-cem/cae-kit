#ifndef WIREDIALOG_H
#define WIREDIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_WireDialog.h"
#include "DataBase.h"

class WireDialog : public QDialog
{
	Q_OBJECT

public:
	
	WireDialog(QWidget *parent = Q_NULLPTR);
	~WireDialog() {}

	Ui::WireDialog ui;

	void fn_AddWirePorts(std::vector<WirePort> &vWire, QTreeWidgetItem *pRoot);
	void fn_EditWirePort(std::vector<WirePort> &vWire, QTreeWidgetItem *pNode, int &iGap);

private:
	std::vector<WirePort> *m_vWire{ nullptr };
	QTreeWidgetItem *m_pWireNode{ nullptr };
	int m_iCurWire{ -1 };

	void fn_SetWireValue(WirePort &wire);
	bool fn_IsNameExist(const std::string &sName, int iSkip);

private slots:
	void fn_AddWireClose();
	void fn_AddWireApply();
	void fn_EditWireClose();
};

#endif