#include "DrawFarDlg.h"

DrawFarDlg::DrawFarDlg(bool b2D, QWidget *parent)
{
	ui.setupUi(this);
	ui.electricPanel->setHidden(true);
	ui.componenPanel->setHidden(true);
	ui.real->setDisabled(true);
	ui.imag->setDisabled(true);
	ui.phase->setDisabled(true);
	if (b2D) {
		this->setWindowTitle("Add 2D far field");
		auto indep = new QLabel("Angle axis");
		axis = new QLabel("Phi");
		ui.gridHead->addWidget(indep, 4, 1);
		ui.gridHead->addWidget(axis, 5, 1);
		thetaOrPhi = new QComboBox;
		thetaOrPhi->addItem("Theta");
		thetaOrPhi->addItem("Phi");
		angle = new QMultiComboBox;
		ui.gridHead->addWidget(thetaOrPhi, 4, 2);
		ui.gridHead->addWidget(angle, 5, 2);
		ui.cart->setChecked(true);
		ui.contour->setHidden(true);
		connect(thetaOrPhi, SIGNAL(currentIndexChanged(int)), this, SLOT(fn_ChangeAxis()));
	}
	layout()->setSizeConstraint(QLayout::SetFixedSize);
	m_b2D = b2D;

	connect(ui.quantity, SIGNAL(currentIndexChanged(int)), this, SLOT(fn_ChangePart()));
	connect(ui.absE,  SIGNAL(clicked()), this, SLOT(fn_ClickE_total()));
	connect(ui.Eth,   SIGNAL(clicked()), this, SLOT(fn_ClickE_total()));
	connect(ui.Eph,   SIGNAL(clicked()), this, SLOT(fn_ClickE_total()));
	connect(ui.mag,   SIGNAL(clicked()), this, SLOT(fn_Select_dB()));
	connect(ui.real,  SIGNAL(clicked()), this, SLOT(fn_Select_dB()));
	connect(ui.imag,  SIGNAL(clicked()), this, SLOT(fn_Select_dB()));
	connect(ui.phase, SIGNAL(clicked()), this, SLOT(fn_Select_dB()));

	this->setFixedSize(this->width(), this->height());
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_GetFarPara()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

DrawFarDlg::~DrawFarDlg()
{
	delete thetaOrPhi;
	delete angle;
	delete axis;
}

void DrawFarDlg::fn_ShowDialog(PostData &post, FarSet &far)
{
	for (int i = 1; i <= post.m_iNumVolt; ++i) {
		ui.voltIndex->addItem(QString::number(i));
	}
	for (auto &far : post.m_vFarPara) {
		ui.farIndex->addItem(QString::fromStdString(far.sName));
	}
	for (auto &freq : post.m_vFreq) {
		ui.freqIndex->addItem(QString::number(freq));
	}

	if (post.m_bRCS) {
		ui.quantity->addItem("RCS");
	} else {
		ui.quantity->addItem("Gain");
		ui.quantity->addItem("Directivity");
	}
	ui.quantity->addItem("Axial Ratio");
	ui.quantity->setCurrentIndex(1);

	if (m_b2D) {
		for (auto &ph : post.m_vFarPara[0].vPhi) {
			angle->addItem(QString::number(ph));
		}
		angle->SetDisplayText("SelectAngle");
	}

	far.bCancel = true;
	far.bGrf2D = m_b2D;
	m_post = &(post);
	m_pFarSet = &(far);

	this->exec();
}

void DrawFarDlg::fn_ChangeAxis()
{
	int m = ui.farIndex->currentIndex();
	int n = thetaOrPhi->currentIndex();

	delete angle;
	angle = new QMultiComboBox;
	angle->SetDisplayText("SelectAngle");
	ui.gridHead->addWidget(angle, 5, 2);
	if ("Theta" == thetaOrPhi->currentText()) {
		axis->setText("Phi");
		for (auto &ph : m_post->m_vFarPara[0].vPhi) {
			angle->addItem(QString::number(ph));
		}
	} else {
		axis->setText("Theta");
		for (auto &th : m_post->m_vFarPara[0].vTheta) {
			angle->addItem(QString::number(th));
		}
	}
}

void DrawFarDlg::fn_GetFarPara()
{
	m_pFarSet->iVoltIdx = ui.voltIndex->currentIndex();
	m_pFarSet->iFarIndx = ui.farIndex->currentIndex();
	m_pFarSet->iFrqIndx = ui.freqIndex->currentIndex();
	m_pFarSet->sQuantity = ui.quantity->currentText();

	if ("Electric Field" == m_pFarSet->sQuantity) {
		if (ui.absE->isChecked()) {
			m_pFarSet->sComponent = "Abs";
		} else if (ui.Eth->isChecked()) {
			m_pFarSet->sComponent = "Theta";
		} else {
			m_pFarSet->sComponent = "Phi";
		}

		if (ui.real->isChecked()) {
			m_pFarSet->sPart = "Real";
		} else if (ui.imag->isChecked()) {
			m_pFarSet->sPart = "Image";
		} else if (ui.mag->isChecked()) {
			m_pFarSet->sPart = "Mag";
		} else {
		    m_pFarSet->sPart = "Phase";
	    }
	} else {
		if (ui.absG->isChecked()) {
			m_pFarSet->sComponent = "Abs";
		} else if (ui.Gth->isChecked()) {
			m_pFarSet->sComponent = "Theta";
		} else if (ui.Gph->isChecked()) {
			m_pFarSet->sComponent = "Phi";
		} else if (ui.Glhc->isChecked()) {
			m_pFarSet->sComponent = "LHC";
		} else {
			m_pFarSet->sComponent = "RHC";
		}
	}

	if (ui.polar->isChecked()) {
		m_pFarSet->iCoordinate = 0;
	} else if (ui.cart->isChecked()) {
		m_pFarSet->iCoordinate = 1;
	} else if (ui.contour->isChecked()) {
		m_pFarSet->iCoordinate = 2;
	}

	if (ui.norm->isChecked()) {
		m_pFarSet->bNormlize = true;
	} else {
		m_pFarSet->bNormlize = false;
	}

	if (ui.dB->isChecked()) {
		m_pFarSet->bSetdB = true;
	} else {
		m_pFarSet->bSetdB = false;
	}

	m_pFarSet->bCancel = false;
	m_pFarSet->bGrf2D = m_b2D;
	if (m_b2D) {
		m_pFarSet->bTheta = ("Theta" == thetaOrPhi->currentText());
		m_pFarSet->vAngleIndx.clear();
		for (int i = 0; i < angle->count(); ++i) {
			if (angle->itemData(i).toBool()) {
				m_pFarSet->vAngleIndx.push_back(i);
			}
		}
	}
	this->reject();
}

void DrawFarDlg::fn_ChangePart()
{
	auto sName = ui.quantity->currentText();

	if ("Electric Field" == sName) {
		ui.electricPanel->setVisible(true);
		ui.componenPanel->setVisible(true);
		ui.gainPanel->setHidden(true);
	} else if ("Axial Ratio" == sName) {
		ui.electricPanel->setHidden(true);
		ui.componenPanel->setHidden(true);
		ui.gainPanel->setHidden(true);
	} else {
		ui.electricPanel->setHidden(true);
		ui.componenPanel->setHidden(true);
		ui.gainPanel->setTitle(ui.quantity->currentText());
		ui.gainPanel->setVisible(true);
	}
}

void DrawFarDlg::fn_ClickE_total()
{
	if (ui.absE->isChecked()) {
		ui.mag->setChecked(true);
		ui.real->setDisabled(true);
		ui.imag->setDisabled(true);
		ui.phase->setDisabled(true);
	} else {
		ui.real->setEnabled(true);
		ui.imag->setEnabled(true);
		ui.phase->setEnabled(true);
	}

	if (ui.mag->isChecked()) {
		ui.dB->setEnabled(true);
	} else {
		ui.dB->setChecked(false);
		ui.dB->setDisabled(true);
	}
}

void DrawFarDlg::fn_Select_dB()
{
	if (ui.mag->isChecked()) {
		ui.dB->setEnabled(true);
	} else {
		ui.dB->setChecked(false);
		ui.dB->setDisabled(true);
	}
}

