#ifndef DRAW_NETWORK_DIALOG_H
#define DRAW_NETWORK_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include "ui_DrawNetDlg.h"
#include "PostData.h"
#include "vtkTable.h"

class DrawNetDlg : public QDialog
{
	Q_OBJECT

public:
	DrawNetDlg(QWidget *parent = Q_NULLPTR, int iNumPort = 0);
	~DrawNetDlg() {}

	Ui::DrawNetDlg ui;

	void fn_ShowDialog(NetSet &para);
	vtkTable *fn_CreateTable(std::string &fname, bool &ok);

private:
	int m_iNumPort{ 0 };
	NetSet* m_pNetSet{ nullptr };

public slots :
	void fn_GetNetPara();
	void fn_ChangePara();
	void fn_ChangePart();
	void fn_InsertSelect();
	void fn_RemoveSelect();
};

#endif
