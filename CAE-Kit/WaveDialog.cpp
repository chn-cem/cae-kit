#include <QtWidgets/QMessageBox>
#include "WaveDialog.h"

WaveDialog::WaveDialog(QWidget *parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->width(), this->height());

	connect(ui.linear, SIGNAL(clicked()), this, SLOT(fn_CheckRotation()));
	connect(ui.right,  SIGNAL(clicked()), this, SLOT(fn_CheckRotation()));
	connect(ui.left,   SIGNAL(clicked()), this, SLOT(fn_CheckRotation()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply,  SIGNAL(clicked()), this, SLOT(fn_AddPlaneApply()));
}

void WaveDialog::fn_AddPlaneWaves(std::vector<PlanWave> &vWave, QTreeWidgetItem *pRoot)
{
	setWindowTitle("Add Plane Wave");

	m_vWave = &vWave;
	m_pWaveNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "Plane" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddPlaneClose()));
	this->show();
}

void WaveDialog::fn_EditPlaneWave(std::vector<PlanWave> &vWave, QTreeWidgetItem *pNode, int & iWave)
{
	setWindowTitle("Edit Plane Wave");
	ui.name->setText(QString::fromStdString(vWave[iWave].sName));
	ui.minPh->setText(QString::number(vWave[iWave].dMinPh));
	ui.maxPh->setText(QString::number(vWave[iWave].dMaxPh));
	ui.minTh->setText(QString::number(vWave[iWave].dMinTh));
	ui.maxTh->setText(QString::number(vWave[iWave].dMaxTh));
	ui.numPh->setText(QString::number(vWave[iWave].iNumPh));
	ui.numTh->setText(QString::number(vWave[iWave].iNumTh));
	ui.polar->setText(QString::number(vWave[iWave].dPolar));
	ui.OAR->setText(QString::number(vWave[iWave].dOAR));
	if (1 == vWave[iWave].iRot) {
		ui.right->setChecked(true);
		ui.OAR->setEnabled(true);
	} else if (-1 == vWave[iWave].iRot) {
		ui.left->setChecked(true);
		ui.OAR->setEnabled(true);
	} else {
		ui.linear->setChecked(true);
		ui.OAR->setDisabled(true);
	}
	ui.apply->setDisabled(true);

	m_vWave = &vWave;
	m_pWaveNode = pNode;
	m_iCurWave = iWave;

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditPlanClose()));
	this->show();
}

void WaveDialog::fn_CheckRotation()
{
	if (ui.linear->isChecked()) {
		ui.OAR->setDisabled(true);
	} else {
		ui.OAR->setEnabled(true);
	}
}

void WaveDialog::fn_AddPlaneClose()
{
	PlanWave wave;

	fn_SetPlaneValue(wave);
	if (!fn_IsNameExist(wave.sName, -1)) {
		m_vWave->push_back(wave);

		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(wave.sName));
		m_pWaveNode->addChild(pSon);

		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of plane wave has been exist.");
	}
}

void WaveDialog::fn_AddPlaneApply()
{
	PlanWave wave;

	fn_SetPlaneValue(wave);
	if (!fn_IsNameExist(wave.sName, -1)) {
		m_vWave->push_back(wave);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(wave.sName));
		m_pWaveNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "Plane" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of plane wave has been exist.");
	}
}

void WaveDialog::fn_EditPlanClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCurWave)) {
		fn_SetPlaneValue((*m_vWave)[m_iCurWave]);
		m_pWaveNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of plane wave has been exist.");
	}
}

void WaveDialog::fn_SetPlaneValue(PlanWave &wave)
{
	wave.sName = ui.name->text().toStdString();
	wave.dMinPh = ui.minPh->text().toDouble();
	wave.dMaxPh = ui.maxPh->text().toDouble();
	wave.dMinTh = ui.minTh->text().toDouble();
	wave.dMaxTh = ui.maxTh->text().toDouble();
	wave.iNumPh = ui.numPh->text().toInt();
	wave.iNumTh = ui.numTh->text().toInt();
	wave.dPolar = ui.polar->text().toDouble();
	if (ui.right->isChecked()) {
		wave.iRot = 1;
		wave.dOAR = ui.OAR->text().toDouble();
	} else if (ui.left->isChecked()) {
		wave.iRot = -1;
		wave.dOAR = ui.OAR->text().toDouble();
	} else {
		wave.iRot = 0;
		wave.dOAR = 0;
	}
}

bool WaveDialog::fn_IsNameExist(const std::string &sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vWave).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vWave)[i].sName == sName) {
			return true;
		}
	}
	return false;
}
