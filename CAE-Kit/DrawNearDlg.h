#ifndef DRAW_NEAR_FIELD_DIALOG_H
#define DRAW_NEAR_FIELD_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_DrawNfcDlg.h"
#include "./ThirdParty/QMultiComboBox.h"
#include "PostData.h"

class DrawNearDlg : public QDialog
{
	Q_OBJECT

public:
	DrawNearDlg(QWidget *parent = Q_NULLPTR);
	~DrawNearDlg();

	Ui::DrawNfcDlg ui;

	void fn_ShowDialog(PostData &post, NearSet &near);

private:
	QMultiComboBox *nearIndex{ nullptr };
	PostData *m_post{ nullptr };
	NearSet *m_pNearSet{ nullptr };


public slots:
	void fn_GetNearPara();
	void fn_SelectEorH();
	void fn_ChangePart();
};

#endif