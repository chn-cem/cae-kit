#include <QtWidgets/QMessageBox>
#include "WireDialog.h"

WireDialog::WireDialog(QWidget *parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->width(), this->height());

	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply,  SIGNAL(clicked()), this, SLOT(fn_AddWireApply()));
}

void WireDialog::fn_AddWirePorts(std::vector<WirePort> &vWire, QTreeWidgetItem *pRoot)
{
	setWindowTitle("Add Wire Port");

	m_vWire = &vWire;
	m_pWireNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "WGap" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddWireClose()));
	this->show();
}

void WireDialog::fn_EditWirePort(std::vector<WirePort> &vWire, QTreeWidgetItem *pNode, int &iGap)
{
	setWindowTitle("Edit Wire Port");
	ui.name->setText(QString::fromStdString(vWire[iGap].sName));
	ui.x->setText(QString::number(vWire[iGap].Oc.x));
	ui.y->setText(QString::number(vWire[iGap].Oc.y));
	ui.z->setText(QString::number(vWire[iGap].Oc.z));
	ui.ex->setText(QString::number(vWire[iGap].ei.x));
	ui.ey->setText(QString::number(vWire[iGap].ei.y));
	ui.ez->setText(QString::number(vWire[iGap].ei.z));
	ui.apply->setDisabled(true);

	m_vWire = &vWire;
	m_pWireNode = pNode;
	m_iCurWire = iGap;

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditWireClose()));
	this->show();
}

void WireDialog::fn_AddWireClose()
{
	WirePort wire;

	fn_SetWireValue(wire);
	if (!fn_IsNameExist(wire.sName, -1)) {
		m_vWire->push_back(wire);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(wire.sName));
		m_pWireNode->addChild(pSon);
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of wire port has been exist.");
	}
}

void WireDialog::fn_AddWireApply()
{
	WirePort wire;

	fn_SetWireValue(wire);
	if (!fn_IsNameExist(wire.sName, -1)) {
		m_vWire->push_back(wire);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(wire.sName));
		m_pWireNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "WGap" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of wire port has been exist.");
	}
}

void WireDialog::fn_EditWireClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCurWire)) {
		fn_SetWireValue((*m_vWire)[m_iCurWire]);
		m_pWireNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of wire port has been exist.");
	}
}

void WireDialog::fn_SetWireValue(WirePort &wire)
{
	wire.sName = ui.name->text().toStdString();
	wire.Oc.x = ui.x->text().toDouble();
	wire.Oc.y = ui.y->text().toDouble();
	wire.Oc.z = ui.z->text().toDouble();
	wire.ei.x = ui.ex->text().toDouble();
	wire.ei.y = ui.ey->text().toDouble();
	wire.ei.z = ui.ez->text().toDouble();
}

bool WireDialog::fn_IsNameExist(const std::string &sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vWire).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vWire)[i].sName == sName) {
			return true;
		}
	}
	return false;
}
