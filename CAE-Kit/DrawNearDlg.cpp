#include "DrawNearDlg.h"

DrawNearDlg::DrawNearDlg(QWidget *parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->width(), this->height());

	connect(ui.E, SIGNAL(toggled(bool)), this, SLOT(fn_SelectEorH()));
	connect(ui.H, SIGNAL(toggled(bool)), this, SLOT(fn_SelectEorH()));
	connect(ui.absE, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.Ex,   SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.Ey,   SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.Ez,   SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.mag,  SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.pha,  SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.real, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.imag, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.instantMag, SIGNAL(toggled(bool)), this, SLOT(fn_ChangePart()));
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_GetNearPara()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

DrawNearDlg::~DrawNearDlg()
{
	delete nearIndex;
}

void DrawNearDlg::fn_ShowDialog(PostData &post, NearSet &near)
{
	for (int i = 1; i <= post.m_iNumVolt; ++i) {
		ui.voltIndex->addItem(QString::number(i));
	}
	for (auto &freq : post.m_vFreq) {
		ui.freqIndex->addItem(QString::number(freq));
	}
	nearIndex = new QMultiComboBox(this);
	nearIndex->SetDisplayText("SelectFace");
	for (auto &sName : post.m_vNearName) {
		nearIndex->addItem(QString::fromStdString(sName), QVariant(true));
	}
	ui.gridHead->addWidget(nearIndex, 2, 2);
	
	near.bCancel = true;
	m_post = &(post);
	m_pNearSet = &(near);

	this->exec();
}

void DrawNearDlg::fn_SelectEorH()
{
	if (ui.E->isChecked()) {
		ui.absE->setText("|E|");
		ui.Ex->setText("Ex");
		ui.Ey->setText("Ey");
		ui.Ez->setText("Ez");
	} else {
		ui.absE->setText("|H|");
		ui.Ex->setText("Hx");
		ui.Ey->setText("Hy");
		ui.Ez->setText("Hz");
	}
}

void DrawNearDlg::fn_ChangePart()
{
	if (ui.absE->isChecked()) {
		ui.real->setDisabled(true);
		ui.imag->setDisabled(true);
		ui.pha->setDisabled(true);
		ui.mag->setEnabled(true);
		ui.instantMag->setEnabled(true);
		ui.norm->setEnabled(true);
		ui.dB->setEnabled(true);
		if ((!ui.mag->isChecked()) && (!ui.instantMag->isChecked())) {
			ui.instantMag->setChecked(true);
		}
	} else {
		ui.instantMag->setEnabled(true);
		ui.mag->setEnabled(true);
		ui.real->setEnabled(true);
		ui.imag->setEnabled(true);
		ui.pha->setEnabled(true);
		if (ui.mag->isChecked() || ui.instantMag->isChecked()) {
			ui.norm->setEnabled(true);
			ui.dB->setEnabled(true);
		} else {
			ui.norm->setDisabled(true);
			ui.norm->setChecked(false);
			ui.dB->setDisabled(true);
			ui.dB->setChecked(false);
		}
	}
}

void DrawNearDlg::fn_GetNearPara()
{
	m_pNearSet->iVoltIndx = ui.voltIndex->currentIndex();
	m_pNearSet->iFreqIndx = ui.freqIndex->currentIndex();
	m_pNearSet->vNearIndx.clear();
	for (auto i = 0; i < nearIndex->count(); ++i) {
		if (nearIndex->itemData(i).toBool()) {
			m_pNearSet->vNearIndx.push_back(i);
		}
	}

	if (ui.E->isChecked()) {
		m_pNearSet->bIsE = true;
	} else {
		m_pNearSet->bIsE = false;
	}

	if (ui.absE->isChecked()) {
		m_pNearSet->sComponent = "Abs";
	} else if (ui.Ex->isChecked()) {
		m_pNearSet->sComponent = "X";
	} else if (ui.Ey->isChecked()) {
		m_pNearSet->sComponent = "Y";
	} else {
		m_pNearSet->sComponent = "Z";
	}

	if (ui.real->isChecked()) {
		m_pNearSet->sPart = "Real";
	} else if (ui.imag->isChecked()) {
		m_pNearSet->sPart = "Image";
	} else if (ui.mag->isChecked()) {
		m_pNearSet->sPart = "Mag";
	} else if (ui.pha->isChecked()) {
		m_pNearSet->sPart = "Phase";
	} else {
		m_pNearSet->sPart = "Instant";
	}

	if (ui.norm->isChecked()) {
		m_pNearSet->bNormlize = true;
	} else {
		m_pNearSet->bNormlize = false;
	}

	if (ui.dB->isChecked()) {
		m_pNearSet->bSetdB = true;
	} else {
		m_pNearSet->bSetdB = false;
	}

	m_pNearSet->bCancel = false;
	this->reject();
}

