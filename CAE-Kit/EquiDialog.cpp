#include <QtWidgets/QMessageBox>
#include "EquiDialog.h"

EquiDialog::EquiDialog(QWidget *parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->width(), this->height());

	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply,  SIGNAL(clicked()), this, SLOT(fn_AddEquiApply()));
}

void EquiDialog::fn_AddEquivSrc(std::vector<EquivSrc> &vEqu, QTreeWidgetItem *pRoot)
{
	setWindowTitle("Add Equivalent Source");

	m_vEqu = &vEqu;
	m_pEquNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "Equi" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddEquiClose()));
	this->show();
}

void EquiDialog::fn_EditEquivSrc(std::vector<EquivSrc> &vEqu, QTreeWidgetItem *pNode, int &iEqu)
{
	setWindowTitle("Edit Equivalent Source");
	ui.name->setText(QString::fromStdString(vEqu[iEqu].sName));
	ui.x->setText(QString::number(vEqu[iEqu].Oc.x));
	ui.y->setText(QString::number(vEqu[iEqu].Oc.y));
	ui.z->setText(QString::number(vEqu[iEqu].Oc.z));
	ui.theta->setText(QString::number(vEqu[iEqu].theta));
	ui.phi->setText(QString::number(vEqu[iEqu].phi));
	ui.alpha->setText(QString::number(vEqu[iEqu].alpha));
	ui.apply->setDisabled(true);

	m_vEqu = &vEqu;
	m_pEquNode = pNode;
	m_iCurEqui = iEqu;

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditEquiClose()));
	this->show();
}

void EquiDialog::fn_AddEquiClose()
{
	EquivSrc equ;

	fn_SetEquiValue(equ);
	if (!fn_IsNameExist(equ.sName, -1)) {
		m_vEqu->push_back(equ);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(equ.sName));
		m_pEquNode->addChild(pSon);
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of wire port has been exist.");
	}
}

void EquiDialog::fn_AddEquiApply()
{
	EquivSrc equ;

	fn_SetEquiValue(equ);
	if (!fn_IsNameExist(equ.sName, -1)) {
		m_vEqu->push_back(equ);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(equ.sName));
		m_pEquNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "Equi" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of wire port has been exist.");
	}
}

void EquiDialog::fn_EditEquiClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCurEqui)) {
		fn_SetEquiValue((*m_vEqu)[m_iCurEqui]);
		m_pEquNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of wire port has been exist.");
	}
}

void EquiDialog::fn_SetEquiValue(EquivSrc &equ)
{
	equ.sName = ui.name->text().toStdString();
	equ.Oc.x = ui.x->text().toDouble();
	equ.Oc.y = ui.y->text().toDouble();
	equ.Oc.z = ui.z->text().toDouble();
	equ.theta = ui.theta->text().toDouble();
	equ.phi = ui.phi->text().toDouble();
	equ.alpha = ui.alpha->text().toDouble();
}

bool EquiDialog::fn_IsNameExist(const std::string &sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vEqu).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vEqu)[i].sName == sName) {
			return true;
		}
	}
	return false;
}
