#include <QtWidgets/QMessageBox>
#include "MediDialog.h"

MediDialog::MediDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->setFixedSize( this->width (),this->height ());

	connect(ui.tanCheck, SIGNAL(clicked()), this, SLOT(fn_MediLossType()));
	connect(ui.rhoCheck, SIGNAL(clicked()), this, SLOT(fn_MediLossType()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply,  SIGNAL(clicked()), this, SLOT(fn_AddMediApply()));
}

void MediDialog::fn_MediLossType()
{
	if (ui.tanCheck->isChecked()) {
		ui.tanE->setEnabled(true);
		ui.rho->setDisabled(true);
		ui.rho->setText("0");
	} else {
		ui.tanE->setDisabled(true);
		ui.rho->setEnabled(true);
		ui.tanE->setText("0");
	}
}


void MediDialog::fn_AddMaterial(std::vector<Material> &vMedi, QTreeWidgetItem *pRoot)
{
	setWindowTitle("Add Material");

	m_vMedi = &vMedi;
	m_pMediNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "Medium" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddMediClose()));
	this->show();
}

void MediDialog::fn_EditMaterial(std::vector<Material> &vMedi, QTreeWidgetItem *pNode, int &iIndx)
{
	setWindowTitle("Edit Material");
	ui.name->setText(QString::fromStdString(vMedi[iIndx].sName));
	ui.mediId->setText(QString::number(vMedi[iIndx].iID));
	ui.er->setText(QString::number(vMedi[iIndx].dEpsr));
	ui.tanE->setText(QString::number(vMedi[iIndx].dTanE));
	ui.rho->setText(QString::number(vMedi[iIndx].dSgma));
	ui.ur->setText(QString::number(vMedi[iIndx].dMiur));
	ui.tanU->setText(QString::number(vMedi[iIndx].dTanM));
	ui.apply->setDisabled(true);
	if (vMedi[iIndx].dSgma > 1.0e-8) {
		ui.rhoCheck->setChecked(true);
		ui.rho->setEnabled(true);
		ui.tanE->setDisabled(true);
	} else {
		ui.tanCheck->setChecked(true);
		ui.tanE->setEnabled(true);
		ui.rho->setDisabled(true);
	}
	if (vMedi[iIndx].sName == "PEC") {
		ui.name->setDisabled(true);
		ui.er->setDisabled(true);
		ui.ur->setDisabled(true);
		ui.rho->setDisabled(true);
		ui.tanE->setDisabled(true);
		ui.tanU->setDisabled(true);
	} else if (vMedi[iIndx].sName == "Vacuum") {
		ui.name->setDisabled(true);
	} else {
		ui.name->setEnabled(true);
		ui.er->setEnabled(true);
		ui.ur->setEnabled(true);
		ui.rho->setEnabled(true);
		ui.tanE->setEnabled(true);
		ui.tanU->setEnabled(true);
	}

	m_vMedi = &vMedi;
	m_pMediNode = pNode;
	m_iCur = iIndx;

	activateWindow();
	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditMediClose()));
	this->show();
}

void MediDialog::fn_AddMediClose()
{
	Material medi;

	fn_SetMediValue(medi);
	if (!fn_IsNameExist(medi.sName, -1)) {
		m_vMedi->push_back(medi);
		// insert item in the tree
		auto pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(medi.sName));
		m_pMediNode->addChild(pSon);
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of material has been exist.");
	}
}

void MediDialog::fn_AddMediApply()
{
	Material wire;

	fn_SetMediValue(wire);
	if (!fn_IsNameExist(wire.sName, -1)) {
		m_vMedi->push_back(wire);
		// insert item in the tree
		auto pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(wire.sName));
		m_pMediNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "Medium" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of material has been exist.");
	}
}

void MediDialog::fn_EditMediClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCur)) {
		fn_SetMediValue((*m_vMedi)[m_iCur]);
		m_pMediNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of material has been exist.");
	}
}

void MediDialog::fn_SetMediValue(Material &medi)
{
	medi.iID = ui.mediId->text().toInt();
	medi.sName = ui.name->text().toStdString();
	medi.dEpsr = ui.er->text().toDouble();
	medi.dTanE = ui.tanE->text().toDouble();
	medi.dSgma = ui.rho->text().toDouble();
	medi.dMiur = ui.ur->text().toDouble();
	medi.dTanM = ui.tanU->text().toDouble();
}

bool MediDialog::fn_IsNameExist(const std::string& sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vMedi).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vMedi)[i].sName == sName) {
			return true;
		}
	}
	return false;
}
