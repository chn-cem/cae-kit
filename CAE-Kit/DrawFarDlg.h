#ifndef DRAW_FAR_FIELD_DIALOG_H
#define DRAW_FAR_FIELD_DIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_DrawFarDlg.h"
#include "PostData.h"
#include "./ThirdParty/QMultiComboBox.h"

class DrawFarDlg : public QDialog
{
	Q_OBJECT

public:
	DrawFarDlg(bool b2D, QWidget *parent = Q_NULLPTR);
	~DrawFarDlg();

	Ui::DrawFarDlg ui;
    QLabel *axis{ nullptr };
	QComboBox *thetaOrPhi{ nullptr };
	QMultiComboBox *angle{ nullptr };

	void fn_ShowDialog(PostData &post, FarSet &far);

private:
	bool m_b2D{ false };
	PostData *m_post{ nullptr };
	FarSet *m_pFarSet{ nullptr };


public slots:
	void fn_ChangeAxis();
	void fn_GetFarPara();
	void fn_ChangePart();
	void fn_ClickE_total();
	void fn_Select_dB();
};

#endif