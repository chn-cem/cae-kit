#ifndef SOLVER_DIALOG_H
#define SOLVER_DIALOG_H

#include "ui_SolvDialog.h"
#include "DataBase.h"

class SolvDialog : public QDialog
{
	Q_OBJECT

public:
	SolvDialog(QWidget *parent=Q_NULLPTR);
	~SolvDialog() {};

	void fn_SetSolver(SolvPara &solvSet);

private:
	Ui::SolvDialog ui;
	SolvPara *m_pSolvSet{ nullptr };

private slots:
	void fn_SelectCemAlgo(QString sArg);
	void fn_SelectFemSolv(QString sArg);
	void fn_CheckRotation();
	void fn_UpdateSolver();
};

#endif