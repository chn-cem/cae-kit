#include "DrawMesh.h"
#include "vtkPoints.h"
#include "vtkLine.h"
#include "vtkTetra.h"
#include "vtkCellArray.h"
#include "vtkPointData.h"
#include "vtkCamera.h"
#include "vtkAxesActor.h"
#include "vtkProperty.h"
#include "vtkDataSetMapper.h"
#include "vtkPolyDataMapper.h"
#include "vtkGenericOpenGLRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkInteractorStyleTrackballCamera.h"

DrawMesh::DrawMesh()
{
	m_render = vtkSmartPointer<vtkRenderer>::New();
	m_actor = vtkSmartPointer<vtkActor>::New();
	m_actor->GetProperty()->SetOpacity(0.9);
	m_actor->GetProperty()->SetColor(0.2, 0.63, 0.79); // set the colour of model
	m_actor->GetProperty()->SetAmbient(0.2);           // 设置环境光系数
	m_actor->GetProperty()->SetDiffuse(0.6);           // 设置散射系数
	m_actor->GetProperty()->SetSpecular(0.2);          // 设置高光系数
	m_actor->GetProperty()->SetSpecularPower(20);      // 设置高光的比例参数
}

DrawMesh::~DrawMesh()
{
    if (m_actor != nullptr) m_actor->FastDelete();
	if (m_render != nullptr) m_render->FastDelete();
}

void DrawMesh::fn_CreateMap(FaceMesh &gdm)
{
	//if (m_gdmObj != nullptr) m_gdmObj->Delete();
	// create polydata
	auto points = vtkPoints::New();
	auto lines = vtkCellArray::New();
	auto polys = vtkCellArray::New();

	Node Rmin, Rmax;
	if (gdm.vR.empty()) return;

	Rmax = Rmin = gdm.vR[0];
	for (unsigned i = 0; i < gdm.vR.size(); ++i) {
		points->InsertPoint(i, gdm.vR[i].x, gdm.vR[i].y, gdm.vR[i].z);
		if (gdm.vR[i].x > Rmax.x) Rmax.x = gdm.vR[i].x;
		if (gdm.vR[i].x < Rmin.x) Rmin.x = gdm.vR[i].x;
		if (gdm.vR[i].y > Rmax.y) Rmax.y = gdm.vR[i].y;
		if (gdm.vR[i].y < Rmin.y) Rmin.y = gdm.vR[i].y;
		if (gdm.vR[i].z > Rmax.z) Rmax.z = gdm.vR[i].z;
		if (gdm.vR[i].z < Rmin.z) Rmin.z = gdm.vR[i].z;
	}
	// get the center of the mesh
	m_Oc = (Rmin + Rmax) * 0.5;
	// get the radius of the mesh
	Rmax -= Rmin;
	m_radius = get_length(Rmax) * 0.5;

	vtkIdType V[4];
	// insert wires
	for (auto &wire : gdm.vvWire) {
		V[0] = wire[0] - 1;
		V[1] = wire[1] - 1;
		auto seg = vtkSmartPointer<vtkLine>::New();
		seg->GetPointIds()->SetId(0, V[0]);
		seg->GetPointIds()->SetId(1, V[1]);
		lines->InsertNextCell(seg);
	}
	// insert triangles
	for (auto &tria : gdm.vvTria) {
		V[0] = tria[0] - 1;
		V[1] = tria[1] - 1;
		V[2] = tria[2] - 1;
		polys->InsertNextCell(3, V);
	}
	// insert quadrangles
	for (auto &quad : gdm.vvQuad) {
		V[0] = quad[0] - 1;
		V[1] = quad[1] - 1;
		V[2] = quad[2] - 1;
		V[3] = quad[3] - 1;
		polys->InsertNextCell(4, V);
	}

	m_gdmObj = vtkPolyData::New();
	m_gdmObj->SetPoints(points);
	m_gdmObj->SetLines(lines);
	m_gdmObj->SetPolys(polys);

	auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(m_gdmObj);
	m_actor->SetMapper(mapper);

	points->Delete();
	lines->Delete();
	polys->Delete();
}

void DrawMesh::fn_CreateMap(VoluMesh &tet)
{
	//if (m_tetObj != nullptr) m_gdmObj->Delete();
	// create polydata
	auto points = vtkPoints::New();
	m_tetObj = vtkSmartPointer<vtkUnstructuredGrid>::New();

	Node Rmin, Rmax;
	if (tet.vR.empty()) return;

	Rmax = Rmin = tet.vR[0];
	for (unsigned i = 0; i < tet.vR.size(); ++i) {
		points->InsertPoint(i, tet.vR[i].x, tet.vR[i].y, tet.vR[i].z);
		if (tet.vR[i].x > Rmax.x) Rmax.x = tet.vR[i].x;
		if (tet.vR[i].x < Rmin.x) Rmin.x = tet.vR[i].x;
		if (tet.vR[i].y > Rmax.y) Rmax.y = tet.vR[i].y;
		if (tet.vR[i].y < Rmin.y) Rmin.y = tet.vR[i].y;
		if (tet.vR[i].z > Rmax.z) Rmax.z = tet.vR[i].z;
		if (tet.vR[i].z < Rmin.z) Rmin.z = tet.vR[i].z;
	}
	// get the center of the mesh
	m_Oc = (Rmin + Rmax) * 0.5;
	// get the radius of the mesh
	Rmax -= Rmin;
	m_radius = get_length(Rmax) * 0.5;

	m_tetObj->SetPoints(points);
	// insert tetr.
	for (auto &tetr : tet.vvTetr) {
		auto vtkTet = vtkSmartPointer<vtkTetra>::New();
		vtkTet->GetPointIds()->SetId(0, tetr[0] - 1);
		vtkTet->GetPointIds()->SetId(1, tetr[1] - 1);
		vtkTet->GetPointIds()->SetId(2, tetr[2] - 1);
		vtkTet->GetPointIds()->SetId(3, tetr[3] - 1);
		m_tetObj->InsertNextCell(VTK_TETRA, vtkTet->GetPointIds());
	}

	auto mapper = vtkSmartPointer<vtkDataSetMapper>::New();
	mapper->SetInputData(m_tetObj);
	m_actor->SetMapper(mapper);

	points->Delete();
}

void DrawMesh::fn_RenderWin(QVTKOpenGLNativeWidget *qvtkWidget)
{
	// rendering
	m_render->SetBackground(1.0, 1.0, 1.0);
	m_render->SetBackground2(0.529, 0.8078, 0.92157);
	m_render->SetGradientBackground(1);

	// add camera
	auto camera = vtkSmartPointer<vtkCamera>::New();
	camera->SetPosition(5, -5, 5);
	camera->SetFocalPoint(0, 0, 0);
	camera->SetViewUp(-1, 1, 0);
	m_render->SetActiveCamera(camera);
	m_render->ResetCamera();

	auto renderWin = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
	renderWin->AddRenderer(m_render);
	auto renderWinInt = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWinInt->SetRenderWindow(renderWin);
	qvtkWidget->SetRenderWindow(renderWin);

	// add coordinate axes
	auto axes = vtkSmartPointer<vtkAxesActor>::New();
	auto widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
	widget->SetOutlineColor(0.93, 0.57, 0.13);
	widget->SetOrientationMarker(axes);
	widget->SetInteractor(renderWinInt);
	widget->SetViewport(0.0, 0.0, 0.15, 0.3);
	widget->SetEnabled(true);
	widget->InteractiveOff();      // forbid moving the coordinate axes

	// turn the camera dirction by mouse
	auto style = vtkInteractorStyleTrackballCamera::New();
	renderWinInt->SetInteractorStyle(style);

	camera->Delete();
	style->Delete();

	renderWin->Render();
	renderWinInt->Initialize();
	renderWinInt->Start();

	//axes->Delete();
	//widget->Delete();
	//m_actor->Delete();
	//m_render->Delete();
	//renderWin->Delete();
	//renderWinInt->Delete();
}

void DrawMesh::fn_UpdateMsh(QVTKOpenGLNativeWidget *qvtkWidget)
{
	fn_ClearActor();
	m_render->AddActor(m_actor);
	m_render->ResetCamera();
	qvtkWidget->GetRenderWindow()->Render();
}

void DrawMesh::fn_ClearMesh(QVTKOpenGLNativeWidget *qvtkWidget)
{
	m_render->RemoveActor(m_actor);
	qvtkWidget->GetRenderWindow()->Render();
}

void DrawMesh::fn_ShowHideMsh()
{
	m_actor->GetProperty()->SetRepresentationToSurface();
	m_actor->SetVisibility(!m_actor->GetVisibility());
}

void DrawMesh::fn_ShowGridMsh()
{
	m_actor->GetProperty()->SetRepresentationToWireframe();
	m_actor->SetVisibility(true);
}

void DrawMesh::fn_ShowNodeMsh()
{
	m_actor->GetProperty()->SetRepresentationToPoints();
	m_actor->SetVisibility(true);
}

void DrawMesh::fn_ClearActor()
{
	auto pActors = m_render->GetActors();
	pActors->InitTraversal();
	for (auto i = 0; i < pActors->GetNumberOfItems(); ++i) {
		m_render->RemoveActor(pActors->GetNextActor());
	}
}

vtkSmartPointer<vtkPolyData> DrawMesh::fn_GetGdmMesh()
{
	return m_gdmObj;
}

vtkSmartPointer<vtkUnstructuredGrid> DrawMesh::fn_GetTetMesh()
{
	return m_tetObj;
}

