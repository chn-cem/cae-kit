#ifndef SYMMETRIC_DIALOG_H
#define SYMMETRIC_DIALOG_H

#include "ui_SymmDialog.h"
#include "DataBase.h"

class SymmDialog : public QDialog
{
	Q_OBJECT

public:
	SymmDialog(QWidget *parent = Q_NULLPTR);
	~SymmDialog() {}

	Ui::SymmDialog ui;

	void fn_SetSymmetry(int *pSymSet);

private:
	int *m_pSymSet{ nullptr };

private slots:
	void fn_UpdateSymme();
};

#endif

