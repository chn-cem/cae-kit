#include "FreqDialog.h"

FreqDialog::FreqDialog(QWidget *parent)
{
    ui.setupUi(this);
    this->setFixedSize(this->width(), this->height());

	connect(ui.freqUnit, SIGNAL(currentIndexChanged(QString)), this, SLOT(fn_CheckUnit(QString)));
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_UpdateFreq()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

void FreqDialog::fn_SetFrequency(FreqPara &freqSet)
{
	m_pFreqSet = &freqSet;
	if (freqSet.sFreqUnit == "KHz") {
		ui.freqUnit->setCurrentIndex(0);
		ui.minFreqUnit->setText("KHz");
		ui.maxFreqUnit->setText("KHz");
	} else if (freqSet.sFreqUnit == "MHz") {
		ui.freqUnit->setCurrentIndex(1);
		ui.minFreqUnit->setText("MHz");
		ui.maxFreqUnit->setText("MHz");
	} else {
		ui.freqUnit->setCurrentIndex(2);
		ui.minFreqUnit->setText("GHz");
		ui.maxFreqUnit->setText("GHz");
	}

	ui.freq->setText(QString::number(freqSet.dFreq0));
	if (freqSet.iNumFreq > 0) {
		ui.sweepFreq->setChecked(true);
		ui.minFreq->setText(QString::number(freqSet.dMinFreq));
		ui.maxFreq->setText(QString::number(freqSet.dMaxFreq));
		ui.numFreq->setText(QString::number(freqSet.iNumFreq));
	} else {
		ui.sweepFreq->setChecked(false);
	}

	if ("Discrete" == freqSet.sSwpType) {
		ui.swpType->setCurrentIndex(0);
	} else if ("Interpolate" == freqSet.sSwpType) {
		ui.swpType->setCurrentIndex(1);
	} else {
		ui.swpType->setCurrentIndex(2);
	}
	if (freqSet.bReviseMedi) {
		ui.reviseMedi->setChecked(true);
	} else {
		ui.reviseMedi->setChecked(false);
	}
	this->show();
}

void FreqDialog::fn_UpdateFreq()
{
	m_pFreqSet->dFreq0 = ui.freq->text().toDouble();
	m_pFreqSet->sFreqUnit = ui.freqUnit->currentText().toStdString();
	if (ui.sweepFreq->isChecked()) {
		m_pFreqSet->dMinFreq = ui.minFreq->text().toDouble();
		m_pFreqSet->dMaxFreq = ui.maxFreq->text().toDouble();
		m_pFreqSet->iNumFreq = ui.numFreq->text().toInt();
		m_pFreqSet->sSwpType = ui.swpType->currentText().toStdString();
	} else {
		m_pFreqSet->iNumFreq = 0;
	}
	if (ui.reviseMedi->isChecked()) {
		m_pFreqSet->bReviseMedi = true;
	} else {
		m_pFreqSet->bReviseMedi = false;
	}
	this->reject();
}

void FreqDialog::fn_CheckUnit(QString sUnit)
{
	ui.minFreqUnit->setText(sUnit);
	ui.maxFreqUnit->setText(sUnit);
}
