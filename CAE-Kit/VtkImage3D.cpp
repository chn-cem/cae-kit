
#include <QPixmap>
#include "vtkCamera.h"
#include "vtkProperty.h"
#include "vtkAxesActor.h"
#include "vtkDataSetMapper.h"
#include "vtkGenericOpenGLRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkOrientationMarkerWidget.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkScalarBarActor.h"
#include "vtkTextProperty.h"
#include "vtkBMPWriter.h"
#include "vtkPNGWriter.h"
#include "vtkJPEGWriter.h"
#include "vtkRenderLargeImage.h"
#include "VtkImage3D.h"
#include <QtWidgets/QMessageBox>

VtkImage3D::VtkImage3D(QString &sName)
{
	m_sName = sName;
	m_tab    = new QWidget();
	m_gridLy = new QGridLayout(m_tab);
	m_gridLy->setSpacing(0);
	m_gridLy->setContentsMargins(0, 0, 0, 0);
	
	m_qvtkWidget = new QVTKOpenGLNativeWidget(m_tab);
	m_gridLy->addWidget(m_qvtkWidget, 0, 0, 1, 1);

	m_mapper = nullptr;
	m_actor  = vtkSmartPointer<vtkActor>::New();
	m_render = vtkSmartPointer<vtkRenderer>::New();
}

VtkImage3D::~VtkImage3D( )
{
	fn_Clear();
}

void VtkImage3D::fn_FitView()
{
	m_render->ResetCamera();
	m_qvtkWidget->GetRenderWindow()->Render();
}

void VtkImage3D::fn_SetName(QString &sName)
{
	m_sName = sName;
}

void VtkImage3D::fn_SetMapper(vtkSmartPointer<vtkPolyDataMapper> map)
{
	m_mapper = map;
}

void VtkImage3D::fn_AddGdmMsh(vtkSmartPointer<vtkPolyData> gdm)
{
	auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(gdm);
	auto actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	actor->GetProperty()->SetOpacity(0.9);
	actor->GetProperty()->SetColor(0.2, 0.63, 0.79); // set the colour of model
	actor->GetProperty()->SetAmbient(0.2);           // 设置环境光系数
	actor->GetProperty()->SetDiffuse(0.6);           // 设置散射系数
	actor->GetProperty()->SetSpecular(0.2);          // 设置高光系数
	actor->GetProperty()->SetSpecularPower(20);      // 设置高光的比例参数
	m_render->AddActor(actor);
}

void VtkImage3D::fn_AddTetMsh(vtkSmartPointer<vtkUnstructuredGrid> tet)
{
	auto mapper = vtkSmartPointer<vtkDataSetMapper>::New();
	mapper->SetInputData(tet);
	auto actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	actor->GetProperty()->SetOpacity(0.9);
	actor->GetProperty()->SetColor(0.2, 0.63, 0.79); // set the colour of model
	actor->GetProperty()->SetAmbient(0.2);           // 设置环境光系数
	actor->GetProperty()->SetDiffuse(0.6);           // 设置散射系数
	actor->GetProperty()->SetSpecular(0.2);          // 设置高光系数
	actor->GetProperty()->SetSpecularPower(20);      // 设置高光的比例参数
	m_render->AddActor(actor);
}

void VtkImage3D::fn_ShowHideMsh()
{
	auto pActors = m_render->GetActors();
	pActors->InitTraversal();
	auto actor = pActors->GetNextActor();
	actor->GetProperty()->SetRepresentationToSurface();
	actor->SetVisibility(!actor->GetVisibility());
}

void VtkImage3D::fn_ShowGridMsh()
{
	auto pActors = m_render->GetActors();
	pActors->InitTraversal();
	auto actor = pActors->GetNextActor();
	actor->GetProperty()->SetRepresentationToWireframe();
	actor->SetVisibility(true);
}

void VtkImage3D::fn_ShowNodeMsh()
{
	auto pActors = m_render->GetActors();
	pActors->InitTraversal();
	auto actor = pActors->GetNextActor();
	actor->GetProperty()->SetRepresentationToPoints();
	actor->SetVisibility(true);
}

QVTKOpenGLNativeWidget* VtkImage3D::fn_GetVTKWidget()
{
	return m_qvtkWidget;
}

QString VtkImage3D::fn_GetName()
{
	return m_sName;
}

QWidget* VtkImage3D::fn_GetTab()
{
	return m_tab;
}

void VtkImage3D::fn_ToRender()
{
	// rendering
	m_render->SetBackground(1.0, 1.0, 1.0);              // 设置页面底部颜色值
	m_render->SetBackground2(0.529, 0.8078, 0.92157);    // 设置页面顶部颜色值
	m_render->SetGradientBackground(1);                  // 开启渐变色背景设置
	// add actor
	m_actor->SetMapper(m_mapper);
	m_actor->GetProperty()->SetOpacity(1);
	m_render->AddActor(m_actor);
	// add camera
	auto camera = vtkSmartPointer<vtkCamera>::New(); 
	camera->SetPosition(5, -5, 5);   // 设相机位置
	camera->SetFocalPoint(0, 0, 0);  // 设焦点位置
	camera->SetViewUp(-1, 1, 0);     // 设视角位置
	m_render->SetActiveCamera(camera);
	m_render->ResetCamera();

	auto renderWin = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
	renderWin->AddRenderer(m_render);
	auto renderWinInt = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWinInt->SetRenderWindow(renderWin);
	m_qvtkWidget->SetRenderWindow(renderWin);

	// add coordinate axes
	auto axes = vtkSmartPointer<vtkAxesActor>::New();
	auto widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
	widget->SetOutlineColor(0.93, 0.57, 0.13);
	widget->SetOrientationMarker(axes);
	widget->SetInteractor(renderWinInt);
	widget->SetViewport(0.0, 0.0, 0.15, 0.3);
	widget->SetEnabled(true);
	widget->InteractiveOff();      // forbid moving the coordinate axes

	// add colorbar
	auto scalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
	scalarBar->SetLookupTable(m_mapper->GetLookupTable());
	scalarBar->SetTitle(" ");
	scalarBar->SetWidth(0.08);
	scalarBar->SetHeight(0.6);
	scalarBar->SetNumberOfLabels(10);
	scalarBar->SetLabelFormat("%8.2e");
	auto textprop = vtkSmartPointer<vtkTextProperty>::New(); 
	textprop->SetColor(0,0,0);
	textprop->SetFontSize(12);
	textprop->SetFontFamilyToTimes();
	textprop->SetBold(1);
	textprop->SetJustification(2);
	scalarBar->SetLabelTextProperty(textprop);
	m_render->AddActor2D(scalarBar);

	// turn the camera dirction by mouse
	auto style = vtkInteractorStyleTrackballCamera::New();
	renderWinInt->SetInteractorStyle(style);

	camera->Delete();
	textprop->Delete();
	scalarBar->Delete();
	style->Delete();

	renderWin->Render();
	renderWinInt->Initialize();
	renderWinInt->Start();

	m_render->Delete();
	renderWin->Delete();
	renderWinInt->Delete();
}

void VtkImage3D::fn_SaveGraph(std::string fname)
{
	vtkRenderLargeImage *image = vtkRenderLargeImage::New();
	image->SetInput(m_render);
	image->SetMagnification(1);

	int len = fname.length();
	std::string kind = fname.substr(len - 4);
	if (kind == ".bmp")
	{
		vtkBMPWriter *picOut = vtkBMPWriter::New();
		picOut->SetInputConnection(image->GetOutputPort());
		picOut->SetFileName(fname.c_str());
		picOut->Write();
		picOut->Delete();
	}else if (kind == ".png")
	{
		vtkPNGWriter *picOut = vtkPNGWriter::New();
		picOut->SetInputConnection(image->GetOutputPort());
		picOut->SetFileName(fname.c_str());
		picOut->Write();
		picOut->Delete();
	}else if (kind == ".jpg")
	{
		vtkJPEGWriter *picOut = vtkJPEGWriter::New();
		picOut->SetInputConnection(image->GetOutputPort());
		picOut->SetFileName(fname.c_str());
		picOut->Write();
		picOut->Delete();
	}

	image->Delete();
}


void VtkImage3D::fn_Clear()
{
	m_qvtkWidget->GetRenderWindow()->GetInteractor()->FastDelete();
	m_qvtkWidget->GetRenderWindow()->Finalize();
	m_qvtkWidget->GetRenderWindow()->FastDelete();
	
	m_actor->Delete();
	m_mapper->Delete();
	m_render->Delete();

	// the order of following sentence is very important
	if (nullptr != m_qvtkWidget) delete m_qvtkWidget;
	if (nullptr != m_gridLy) delete m_gridLy;
    if (nullptr != m_tab) delete m_tab;
	m_tab = nullptr;
	m_gridLy = nullptr;
	m_qvtkWidget = nullptr;
}

