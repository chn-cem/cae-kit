#ifndef LUMPDIALOG_H
#define LUMPDIALOG_H

#include <QtWidgets/QTreeWidget>
#include <string>
#include "ui_LumpDialog.h"
#include "DataBase.h"

class LumpDialog : public QDialog
{
	Q_OBJECT

public:
	
	LumpDialog(QWidget *parent = Q_NULLPTR);
	~LumpDialog() {}

	Ui::LumpDialog ui;

	void fn_AddLumpPorts(std::vector<LumpPort> &vLump, QTreeWidgetItem *pRoot);
	void fn_EditLumpPort(std::vector<LumpPort> &vLump, QTreeWidgetItem *pNode, int &iLump);

private:
	std::vector<LumpPort> *m_vLump{ nullptr };
	QTreeWidgetItem *m_pLumpNode{ nullptr };
	int m_iCurLump{ -1 };

	void fn_SetLumpValue(LumpPort &wire);
	bool fn_IsNameExist(const std::string &sName, int iSkip);

private slots:
	void fn_AddLumpClose();
	void fn_AddLumpApply();
	void fn_EditLumpClose();
};

#endif