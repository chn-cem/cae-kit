#include <QtWidgets/QMessageBox>
#include "FarsDialog.h"

FarsDialog::FarsDialog(QWidget *parent)
{
	ui.setupUi(this);
	this->setFixedSize(this->width(), this->height());

	connect(ui.xoz, SIGNAL(clicked()), this, SLOT(fn_QuickSetXOZ()));
	connect(ui.yoz, SIGNAL(clicked()), this, SLOT(fn_QuickSetYOZ()));
	connect(ui.xoy, SIGNAL(clicked()), this, SLOT(fn_QuickSetXOY()));
	connect(ui.sphere, SIGNAL(clicked()), this, SLOT(fn_QuickSetSph()));
	connect(ui.cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.apply,  SIGNAL(clicked()), this, SLOT(fn_AddFarApply()));
}

void FarsDialog::fn_AddFarsSet(std::vector<PreFarsSet> &vFar, QTreeWidgetItem *pRoot)
{
	setWindowTitle("Add Far Field Surface");

	m_vFar = &vFar;
	m_pFarNode = pRoot;

	std::string sName;
	QString str;
	int i = 1;
	while (true) {
		str = "Far" + QString::number(i);
		sName = str.toStdString();
		if (!fn_IsNameExist(sName, -1)) {
			break;
		}
		++i;
	}
	ui.name->setText(str);
	ui.apply->setEnabled(true);

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_AddFarClose()));
	this->show();
}

void FarsDialog::fn_EditFarSet(std::vector<PreFarsSet> &vFar, QTreeWidgetItem *pNode, int &iIndx)
{
	setWindowTitle("Edit Far Field Surface");
	ui.name->setText(QString::fromStdString(vFar[iIndx].sName));
	ui.minPh->setText(QString::number(vFar[iIndx].dMinPh));
	ui.maxPh->setText(QString::number(vFar[iIndx].dMaxPh));
	ui.minTh->setText(QString::number(vFar[iIndx].dMinTh));
	ui.maxTh->setText(QString::number(vFar[iIndx].dMaxTh));
	ui.numPh->setText(QString::number(vFar[iIndx].iNumPh));
	ui.numTh->setText(QString::number(vFar[iIndx].iNumTh));
	ui.apply->setDisabled(true);

	m_vFar = &vFar;
	m_pFarNode = pNode;
	m_iCur = iIndx;

	disconnect(ui.ok, 0, 0, 0);
	connect(ui.ok, SIGNAL(clicked()), this, SLOT(fn_EditFarClose()));
	this->show();
}

void FarsDialog::fn_QuickSetXOZ()
{
	ui.minPh->setText(QString::number(0));
	ui.maxPh->setText(QString::number(0));
	ui.minTh->setText(QString::number(-180));
	ui.maxTh->setText(QString::number(180));
	ui.numPh->setText(QString::number(1));
	ui.numTh->setText(QString::number(361));
}

void FarsDialog::fn_QuickSetYOZ()
{
	ui.minPh->setText(QString::number(90));
	ui.maxPh->setText(QString::number(90));
	ui.minTh->setText(QString::number(-180));
	ui.maxTh->setText(QString::number(180));
	ui.numPh->setText(QString::number(1));
	ui.numTh->setText(QString::number(361));
}

void FarsDialog::fn_QuickSetXOY()
{
	ui.minPh->setText(QString::number(0));
	ui.maxPh->setText(QString::number(360));
	ui.minTh->setText(QString::number(90));
	ui.maxTh->setText(QString::number(90));
	ui.numPh->setText(QString::number(361));
	ui.numTh->setText(QString::number(1));
}

void FarsDialog::fn_QuickSetSph()
{
	ui.minPh->setText(QString::number(0));
	ui.maxPh->setText(QString::number(360));
	ui.minTh->setText(QString::number(0));
	ui.maxTh->setText(QString::number(180));
	ui.numPh->setText(QString::number(361));
	ui.numTh->setText(QString::number(181));
}

void FarsDialog::fn_AddFarClose()
{
	PreFarsSet far;

	fn_SetFarValue(far);
	if (!fn_IsNameExist(far.sName, -1)) {
		m_vFar->push_back(far);

		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(far.sName));
		m_pFarNode->addChild(pSon);

		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of far field set has been exist.");
	}
}

void FarsDialog::fn_AddFarApply()
{
	PreFarsSet far;

	fn_SetFarValue(far);
	if (!fn_IsNameExist(far.sName, -1)) {
		m_vFar->push_back(far);
		// insert item in the tree
		QTreeWidgetItem* pSon = new QTreeWidgetItem();
		pSon->setText(0, QString::fromStdString(far.sName));
		m_pFarNode->addChild(pSon);

		std::string sName;
		QString str;
		int i = 1;
		while (true) {
			str = "Far" + QString::number(i);
			sName = str.toStdString();
			if (!fn_IsNameExist(sName, -1)) {
				break;
			}
			++i;
		}
		ui.name->setText(str);
	} else {
		QMessageBox::information(this, "Warn", "The name of far field set has been exist.");
	}
}

void FarsDialog::fn_EditFarClose()
{
	std::string sName = ui.name->text().toStdString();

	if (!fn_IsNameExist(sName, m_iCur)) {
		fn_SetFarValue((*m_vFar)[m_iCur]);
		m_pFarNode->setText(0, QString::fromStdString(sName));
		this->reject();
	} else {
		QMessageBox::information(this, "Warn", "The name of far field set has been exist.");
	}
}

void FarsDialog::fn_SetFarValue(PreFarsSet &far)
{
	far.sName = ui.name->text().toStdString();
	far.dMinPh = ui.minPh->text().toDouble();
	far.dMaxPh = ui.maxPh->text().toDouble();
	far.dMinTh = ui.minTh->text().toDouble();
	far.dMaxTh = ui.maxTh->text().toDouble();
	far.iNumPh = ui.numPh->text().toInt();
	far.iNumTh = ui.numTh->text().toInt();
}

bool FarsDialog::fn_IsNameExist(const std::string &sName, int iSkip)
{
	for (unsigned i = 0; i < (*m_vFar).size(); ++i) {
		if (i == iSkip) continue;

		if ((*m_vFar)[i].sName == sName) {
			return true;
		}
	}
	return false;
}
